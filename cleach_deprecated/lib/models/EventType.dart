class EventType {
  String title;
  String place;
  String image;
  DateTime start;
  DateTime end;
  String detail;
  EventType.fromJson(Map<String, dynamic> json)
      : title = json['title'],
        place = json['place'],
        image = json['image'],
        start = (json['start'] == null) ? null : DateTime.parse(json['start']),
        end = (json['end'] == null) ? null : DateTime.parse(json['end']),
        detail = json['detail'];
}
