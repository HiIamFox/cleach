import 'package:flutter/material.dart';
import './pages/homePage.dart';
import './pages/lineupPage.dart';
import './pages/mapPage.dart';

import 'package:graphql_flutter/graphql_flutter.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized(); 
  final HttpLink link = HttpLink(
    uri: 'https://us-central1-cleach-festival.cloudfunctions.net/graphql',
  );

  final ValueNotifier<GraphQLClient> client = ValueNotifier(
    GraphQLClient(
      cache: InMemoryCache(),
      link: link,
    ),
  );

  runApp(App(client));
}

class App extends StatefulWidget {
  final ValueNotifier<GraphQLClient> client;
  App(ValueNotifier<GraphQLClient> client) : client = client;
  State<StatefulWidget> createState() {
    return _AppState(client);
  }
}

class _AppState extends State<App> {
  int _selectedIndex = 1;
  final List<Widget> _pages = [
    LineupPage(),
    HomePage(),
    MapPage()
  ];

  final ValueNotifier<GraphQLClient> client;
  _AppState(ValueNotifier<GraphQLClient> client) : client = client;

  @override
  Widget build(BuildContext context) {
    return GraphQLProvider(
      client: client,
      child: CacheProvider(
        child: MaterialApp(
          title: 'Cleach festival',
          theme: ThemeData.dark(),
          home: Scaffold(
            body: _pages[_selectedIndex],
            bottomNavigationBar: BottomNavigationBar(
              type: BottomNavigationBarType.shifting,
              showSelectedLabels: true,
              showUnselectedLabels: false,
              currentIndex: _selectedIndex,
              onTap: (int index) {
                setState(() {
                  _selectedIndex = index;
                });
              },
              items: <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(Icons.timer),
                  title: Text('Lineup'),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.home),
                  title: Text('Domů'),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.map),
                  title: Text('Mapa'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
