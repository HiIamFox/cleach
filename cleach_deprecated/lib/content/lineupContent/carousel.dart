import 'package:cleach/models/EventType.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class Carousel extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _CarouselState();
  }
}

class _CarouselState extends State<Carousel> {
  final PageController _pageController =
      new PageController(viewportFraction: 0.8);

  Stream slides;
  String activeDay = 'Čtvrtek';
  int currentPage = 0;

  @override
  void initState() {
    _pageController.addListener(() {
      int next = _pageController.page.round();

      if (currentPage != next) {
        setState(() {
          currentPage = next;
        });
      }
    });
    super.initState();
  }

  final String getEvents = """
    query getEvents{
      events {
        title
        end
        start
        place
        detail
        image
      }
    }
  """;
  @override
  Widget build(BuildContext context) {
    return Query(
        options: QueryOptions(
          documentNode: gql(getEvents),
        ),
        builder: (QueryResult result,
            {VoidCallback refetch, FetchMore fetchMore}) {
          if (result.hasException) {
            return Text(result.exception.toString());
          }

          if (result.loading) {
            return CircularProgressIndicator();
          }

          List<EventType> repositories = List<EventType>();
          result.data['events'].forEach(
              (eventJson) => repositories.add(EventType.fromJson(eventJson)));

          return PageView.builder(
            controller: _pageController,
            itemCount: repositories.length + 1,
            itemBuilder: (context, int currentIndex) {
              if (currentIndex == 0) {
                return _buildDaySelectionPage();
              } else if (repositories.length >= currentIndex) {
                bool active = currentIndex == currentPage;
                return _buildCarouselPage(
                    repositories[currentIndex - 1], active);
              }
              return null; //Unreachable
            },
          );
        });
  }

  Widget _buildCarouselPage(EventType event, bool active) {
    final double blur = active ? 20 : 5;
    final double offset = active ? 15 : 5;
    final double top = active ? 100 : 200;
    final Color shadowColor =
        active ? Color.fromARGB(100, 0, 0, 0) : Color.fromARGB(30, 0, 0, 0);

    return AnimatedContainer(
      duration: Duration(milliseconds: 200),
      curve: Curves.easeInOut,
      margin: EdgeInsets.only(top: top, bottom: 50, right: 20),
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          image: NetworkImage(
            'https://cleach.cz/wp-content/uploads/2019/09/bbc.jpg',
          ),
        ),
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
            color: shadowColor,
            blurRadius: blur,
            offset: Offset(offset, offset),
          ),
        ],
      ),
      child: _buildPageContent(event),
    );
  }

  Widget _buildPageContent(EventType event) {
    final PageController _contentController = new PageController();
    return ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: PageView(
        onPageChanged: (int) => {},
        controller: _contentController,
        scrollDirection: Axis.vertical,
        children: <Widget>[
          Stack(
            children: <Widget>[
              Positioned.fill(
                child: Image.network(
                  event.image,
                  fit: BoxFit.cover,
                ),
              ),
              Padding(
                padding: EdgeInsets.all(20),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        "${event.start.hour < 10 ? "0" : ""}${event.start.hour}:${event.start.minute < 10 ? "0" : ""}${event.start.minute} - ${event.end.hour < 10 ? "0" : ""}${event.end.hour}:${event.end.minute < 10 ? "0" : ""}${event.end.minute}",
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.white,
                        ),
                      ),
                      Text(
                        event.place,
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      event.title.toUpperCase(),
                      style: TextStyle(
                          fontSize: 40,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                    FlatButton(
                      child: Text(
                        'Vidět více',
                        style: TextStyle(
                          color: Colors.purple,
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),
                      ),
                      onPressed: () => _changeCardPage(_contentController, 1),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Stack(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 100, left: 15, right: 15),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: Text(
                    event.detail,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: FlatButton(
                  textColor: Colors.purple,
                  child: Text(
                    'Zpět',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  ),
                  onPressed: () => _changeCardPage(_contentController, 0),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildDaySelectionPage() {
    return Container(
      padding: EdgeInsets.only(
        right: 40,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Text(
            'Vyberte den',
            style: TextStyle(
              fontSize: 40,
              fontWeight: FontWeight.bold,
            ),
          ),
          _buildButton('Čtvrtek'),
          _buildButton('Pátek'),
          _buildButton('Sobota')
        ],
      ),
    );
  }

  void _changeCardPage(PageController ctrl, page) {
    ctrl.animateToPage(
      page,
      duration: Duration(
        milliseconds: 400,
      ),
      curve: Curves.easeIn,
    );
  }

  Widget _buildButton(String day) {
    Color color = day == activeDay ? Colors.purple : Colors.white;
    Color textColor = day == activeDay ? Colors.white : Colors.purple;
    return FlatButton(
        color: color,
        textColor: textColor,
        child: Text('$day'),
        onPressed: () => {});
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }
}
