import 'package:flutter/material.dart';

class Calendar extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _CalendarState();
  }
}

class _CalendarState extends State<Calendar> {
  List<String> _stages = ["Ride", "Chill", "Fun", "Art"];
  List<Widget> generateStagesColumnChildren() {
    List<Widget> column = new List();
    column.add(
        Container(
          width: 50,
          height: 15,
        ),
    );
    column.addAll(
      List.generate(_stages.length, (index) {
        return Expanded(
          child: Container(
            width: 50,
            child: Center(
              child: Text(_stages[index]),
            ),
          ),
        );
      }),
    );
    return column;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        DecoratedBox(
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              new BoxShadow(
                color: Color.fromARGB(255, 220, 220, 220),
                offset: new Offset(1, 0.0),
                blurRadius: 3,
              ),
            ],
          ),
          child: Column(
            children: generateStagesColumnChildren(),
          ),
        ),
        Expanded(
          child: ListView(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Row(
                    children: List.generate(4, (index) {
                      return Container(
                        width: 200,
                        height: 15,
                        child: Center(
                          child: Text("$index:00"),
                        ),
                      );
                    }),
                  ),
                  Row(
                    children: List.generate(4, (index) {
                      return Container(
                        width: 100,
                        decoration: BoxDecoration(
                          border: Border(
                            right: BorderSide(
                                color: Color.fromARGB(255, 232, 232, 232)),
                          ),
                        ),
                      );
                    }),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
