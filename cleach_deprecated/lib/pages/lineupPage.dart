import 'package:cleach/content/lineupContent/carousel.dart';
import 'package:flutter/material.dart';

class LineupPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _LineupPageState();
  } 
}

class _LineupPageState extends State<LineupPage>{

  @override
  Widget build(BuildContext context) {
    return Carousel();
  }
}
