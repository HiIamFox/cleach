/* React components */
import React from "react";
import Navigation from "../../containers/Navigation";
import EventsTable from "../../components/EventsTable";
//import EventsImageUpload from "../../components/EventsImageUpload";
import AddNewEvent from "../../components/AddNewEvent/AddNewEvent";
/* React components */

const Events: React.FC = () => {
  return (
    <Navigation>
      <>
        <EventsTable />
        <AddNewEvent />
        {/* <EventsImageUpload /> */}
      </>
    </Navigation>
  );
};

export default Events;
