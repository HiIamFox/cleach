import React from "react";
import UsersTable from "../../components/UsersTable";
import Navigation from "../../containers/Navigation";

const Users: React.FC = () => {
  return (
    <Navigation>
      <UsersTable />
    </Navigation>
  );
};

export default Users;
