import React from "react";
import Navigation from "../../containers/Navigation";
import NewsTable from "../../components/NewsTable";
import SendNotification from "../../components/SendNotification";

const NewsAndNotification: React.FC = () => {
  return (
    <Navigation>
      <>
        <NewsTable />
        <SendNotification/>
      </>
    </Navigation>
  );
};

export default NewsAndNotification;
