import React, { useState } from "react";
import Panel from "../../containers/Panel";
import { TextField, Button, Snackbar } from "@material-ui/core";
import useStyles from "./SendNotificationStyles";
import { useMutation } from "@apollo/react-hooks";
import { SEND_NOTIFICATION } from "../../graphql/mutations";
import MuiAlert, { AlertProps } from "@material-ui/lab/Alert";

function Alert(props: AlertProps) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const SendNotification: React.FC = () => {
  const classes = useStyles();
  const [sendNotification] = useMutation(SEND_NOTIFICATION);

  const [title, setTitle] = useState<string>("");
  const [content, setContent] = useState<string>("");

  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  const titleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setTitle(event.target.value as string);
  };

  const contentChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setContent(event.target.value as string);
  };

  const submit = async () => {
    const notification = {
      title,
      detail: content
    };
    await sendNotification({ variables: { notification } });
    setTitle("");
    setContent("");
  };

  return (
    <Panel
      title="Send notification"
      content={
        <>
          <form className={classes.form} noValidate autoComplete="off">
            <TextField
              className={classes.title}
              id="title"
              label="Title"
              value={title}
              onChange={titleChange}
            />
            <TextField
              className={classes.content}
              id="content"
              label="Content"
              value={content}
              onChange={contentChange}
            />
            <Button
              className={classes.sendButton}
              variant="contained"
              size="medium"
              onClick={() => {
                submit();
                handleOpen();
              }}
            >
              Send
            </Button>
          </form>
          <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
            <Alert onClose={handleClose} severity="success">
              Notification successfully sent!
            </Alert>
          </Snackbar>
        </>
      }
    />
  );
};

export default SendNotification;
