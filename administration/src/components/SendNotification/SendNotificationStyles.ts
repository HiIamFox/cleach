import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  form: {
    width: "100%",
    display: "flex",
    justifyContent: "space-evenly"
  },
  title: {
    width: "20%"
  },
  content: {
    width: "70%"
  },
  sendButton: {
    backgroundColor: "#263238",
    color: "#eceff1",
    width: "100px"
  }
});

export default useStyles;
