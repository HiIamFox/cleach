import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  table: {
    width: "100%"
  },
  tableHeader:{
  },
  tableRow:{
    tr: {
      height: "10px"
    },   
    td: {
       height: "auto !important"
    }
  }
});

export default useStyles;
