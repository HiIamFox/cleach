/* React components */
import React, { ReactElement, useState } from "react";
import Panel from "../../containers/Panel";
import EditUserDialog from "../EditUserDialog";
import TableError from "../TableError";
/* React components */

/* Material design */
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Paper,
  IconButton,
  Chip
} from "@material-ui/core";
import Skeleton from "@material-ui/lab/Skeleton";
import { Edit, Face, Person, Security } from "@material-ui/icons";
/* Material design */

/* GraphQL client */
import { useQuery } from "@apollo/react-hooks";
import { GET_USERS } from "../../graphql/queries";
import { UserType, UserRole } from "../../graphql/models/user";
/* GraphQL client */

import tableStyles from "../../styles/tableStyles";

const UsersTable: React.FC = () => {
  const classes = tableStyles();
  const { loading, error, data, refetch, networkStatus } = useQuery(GET_USERS);

  const [selectedUser, setSelectedUser] = useState<UserType | null>(null);
  const [open, setOpen] = useState<boolean>(false);

  const openDialog = (user: UserType) => {
    setSelectedUser(user);
    setOpen(true);
  };

  const handleCloseDialog = () => setOpen(false);

  const createSkeleton = () => {
    const rows: ReactElement[] = [];
    for (let i = 0; i < 4; i++) {
      rows.push(
        <TableRow key={i}>
          <TableCell align="left">
            <Skeleton variant="text" />
          </TableCell>
          <TableCell align="left">
            <Skeleton variant="text" />
          </TableCell>
          <TableCell align="left">
            <Skeleton variant="text" />
          </TableCell>
          <TableCell align="left">
            <Skeleton variant="text" />
          </TableCell>
          <TableCell align="center"></TableCell>
        </TableRow>
      );
    }
    return rows;
  };

  if (error) {
    return (
      <Panel
        title="Users list"
        content={<TableError error={error} refetch={refetch} />}
      ></Panel>
    );
  }

  return (
    <>
      {!!selectedUser ? (
        <EditUserDialog
          handleCloseDialog={handleCloseDialog}
          open={open}
          setOpen={setOpen}
          user={selectedUser}
          refetch={refetch}
        />
      ) : (
        <></>
      )}
      <Panel
        title="Users list"
        content={
          <Paper className={classes.table}>
            <Table size="small" aria-label="simple table">
              <TableHead>
                <TableRow className={classes.tableHeader}>
                  <TableCell align="left">Display name</TableCell>
                  <TableCell align="left">Email</TableCell>
                  <TableCell align="left">Role</TableCell>
                  <TableCell align="left">UID</TableCell>
                  <TableCell align="center">Actions</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {loading || networkStatus === 4
                  ? createSkeleton()
                  : data.getUsers.users.map((user: UserType) => (
                      <TableRow key={user.uid}>
                        <TableCell align="left">{user.displayName}</TableCell>
                        <TableCell align="left"> {user.email}</TableCell>
                        <TableCell align="left">
                          <Chip
                            icon={
                              user.role === UserRole.DEFAULT ? (
                                <Face />
                              ) : user.role === UserRole.MODERATOR ? (
                                <Person />
                              ) : (
                                <Security />
                              )
                            }
                            label={user.role}
                          />
                        </TableCell>
                        <TableCell align="left">{user.uid}</TableCell>
                        <TableCell align="center">
                          <>
                            <IconButton
                              size="small"
                              onClick={() => openDialog(user)}
                            >
                              <Edit />
                            </IconButton>
                          </>
                        </TableCell>
                      </TableRow>
                    ))}
              </TableBody>
            </Table>
          </Paper>
        }
      ></Panel>
    </>
  );
};

export default UsersTable;
