import React, { useCallback, useState } from "react";
import Panel from "../../containers/Panel";
import { useDropzone } from "react-dropzone";
import app from "firebase";
import { Button } from "@material-ui/core";

const EventsImageUpload: React.FC = () => {
  const [image, setImage] = useState<any>(null);
  const onDrop = useCallback(
    ([image]) => {
      setImage(image);
    },
    [setImage]
  );
  const upload = async () => {
    const storageRef = app.storage().ref(`events/${image.name}`);
    await storageRef.put(image);
  };
  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });

  return (
    <Panel
      title="Upload event image"
      content={
        <>
          <div {...getRootProps()}>
            <input {...getInputProps()} />
            {isDragActive ? (
              <p>Drop the files here ...</p>
            ) : (
              <p>Drag 'n' drop some files here, or click to select files</p>
            )}
          </div>
          <Button variant="contained" onClick={upload}>
            Upload
          </Button>
        </>
      }
    />
  );
};

export default EventsImageUpload;
