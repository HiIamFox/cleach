import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  dialogForm: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    height: "160px"
  }
});

export default useStyles;
