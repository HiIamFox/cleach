/* React components */
import React, { useState, useEffect } from "react";
import SmDialog from "../../containers/SmDialog";
/* React components */

/* GraphQL client */
import { useMutation } from "@apollo/react-hooks";
import { EDIT_USER, EDIT_ROLE } from "../../graphql/mutations";
/* GraphQL client */

import useStyles from "./editUserDialogStyles";

/* Material design */
import {
  DialogContent,
  TextField,
  DialogContentText,
  DialogActions,
  Button,
  Select,
  MenuItem
} from "@material-ui/core";
/* Material design */

/* Models */
import { UserType, UserRole } from "../../graphql/models/user";
/* Models */

interface EditUserDialogProps {
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  handleCloseDialog: () => void;
  user: UserType;
  refetch: () => void;
}

const EditUserDialog: React.FC<EditUserDialogProps> = ({
  open,
  setOpen,
  handleCloseDialog,
  user,
  refetch
}: EditUserDialogProps) => {
  const [editUser] = useMutation(EDIT_USER);
  const [editRole] = useMutation(EDIT_ROLE);

  const [displayName, setDisplayName] = useState<string>(
    !!user.displayName ? user.displayName : ""
  );
  const [email, setEmail] = useState<string>(user.email);
  const [role, setRole] = useState<string>(user.role.toString());

  useEffect(() => {
    setDisplayName(!!user.displayName ? user.displayName : "");
    setEmail(user.email);
  }, [user]);

  const nameChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setDisplayName(event.target.value as string);
  };
  const emailChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setEmail(event.target.value as string);
  };
  const roleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setRole(event.target.value as string);
  };

  const submit = async () => {
    const editedUser = {
      uid: user.uid,
      email,
      displayName,
      role: role,
      disabled: user.disabled
    };
    await editRole({ variables: { email, role } });
    await editUser({ variables: { editedUser } });
    refetch();
    handleCloseDialog();
  };

  const classes = useStyles();

  return (
    <SmDialog open={open} setOpen={setOpen} title="Edit user">
      <DialogContent>
        <DialogContentText>Edit selected user.</DialogContentText>
        <div className={classes.dialogForm}>
          <TextField
            id="display-name"
            label="Display name"
            value={!!displayName ? displayName : ""}
            onChange={nameChange}
          />
          <TextField
            id="email"
            label="Email"
            value={!!email ? email : ""}
            onChange={emailChange}
          />
          <Select
            id="role-select"
            value={role}
            fullWidth={true}
            onChange={roleChange}
          >
            <MenuItem value={UserRole.DEFAULT.toString()}>
              {UserRole.DEFAULT}
            </MenuItem>
            <MenuItem value={UserRole.MODERATOR.toString()}>
              {UserRole.MODERATOR}
            </MenuItem>
            <MenuItem value={UserRole.ADMIN.toString()}>
              {UserRole.ADMIN}
            </MenuItem>
          </Select>
        </div>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCloseDialog} color="primary">
          Cancel
        </Button>
        <Button onClick={submit} color="primary">
          Submit
        </Button>
      </DialogActions>
    </SmDialog>
  );
};

export default EditUserDialog;
