import React, { useState } from "react";
import Panel from "../../containers/Panel";
import useStyles from "./AddNewEventStyles";
import {
  TextField,
  Select,
  MenuItem,
  Button,
  Snackbar
} from "@material-ui/core";
import Place from "../../graphql/models/places";
import { useMutation } from "@apollo/react-hooks";
import { ADD_EVENT } from "../../graphql/mutations";
import Alert from "@material-ui/lab/Alert";

const AddNewEvent: React.FC = () => {
  const classes = useStyles();

  const [addEvent] = useMutation(ADD_EVENT);

  const [place, setPlace] = useState<string>("Jízdárna");
  const [detail, setDetail] = useState<string>("");
  const [image, setImage] = useState<string>("");
  const [title, setTitle] = useState<string>("");
  const [startDate, setStartDate] = React.useState<Date | null>(new Date());
  const [endDate, setEndDate] = React.useState<Date | null>(new Date());
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  const handleStartDateChange = (
    event: React.ChangeEvent<{ value: unknown }>
  ) => {
    setStartDate(event.target.value as Date);
  };

  const handleEndDateChange = (
    event: React.ChangeEvent<{ value: unknown }>
  ) => {
    setEndDate(event.target.value as Date);
  };

  const placeChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setPlace(event.target.value as string);
  };
  const detailChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setDetail(event.target.value as string);
  };
  const titleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setTitle(event.target.value as string);
  };
  const imageChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setImage(event.target.value as string);
  };

  const submit = () => {
    const event = {
      title,
      place,
      image,
      end: endDate,
      start: startDate,
      detail
    };
    addEvent({ variables: { event } });
    setTitle("");
    setPlace("");
    setDetail("");
    setImage("");
  };
  return (
    <Panel
      title="Add new Event"
      content={
        <>
          <form className={classes.form}>
            <TextField
              className={classes.field}
              id="title"
              label="Title"
              value={title}
              onChange={titleChange}
            />
            <TextField
              id="startDate"
              label="Start date"
              type="datetime-local"
              className={classes.field}
              defaultValue="2019-05-24T10:30"
              onChange={handleStartDateChange}
              InputLabelProps={{
                shrink: true
              }}
            />
            <TextField
              id="endDate"
              label="End date"
              type="datetime-local"
              className={classes.field}
              defaultValue="2019-05-24T10:30"
              onChange={handleEndDateChange}
              InputLabelProps={{
                shrink: true
              }}
            />
            <Select
              className={classes.field}
              id="place-select"
              value={place}
              onChange={placeChange}
            >
              <MenuItem value={Place.JIZDARNA.toString()}>
                {Place.JIZDARNA}
              </MenuItem>
              <MenuItem value={Place.ZAMEK.toString()}>{Place.ZAMEK}</MenuItem>
              <MenuItem value={Place.CAFE.toString()}>{Place.CAFE}</MenuItem>
            </Select>
          </form>
          <TextField
            className={classes.fieldMax}
            id="image"
            label="ImageURL"
            value={image}
            onChange={imageChange}
          />
          <TextField
            multiline
            rowsMax="3"
            className={classes.fieldMax}
            id="detail"
            label="Detail"
            value={detail}
            onChange={detailChange}
          />
          <Button
            className={classes.sendButton}
            variant="contained"
            size="medium"
            onClick={() => {
              submit();
              handleOpen();
            }}
          >
            Submit
          </Button>
          <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
            <Alert onClose={handleClose} severity="success">
              Event successfully created!
            </Alert>
          </Snackbar>
        </>
      }
    />
  );
};

export default AddNewEvent;
