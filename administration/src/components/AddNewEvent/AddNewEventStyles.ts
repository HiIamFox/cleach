import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  form: {
    width: "90%",
    display: "flex",
    justifyContent: "space-between",
    flexWrap: "wrap"
  },
  field: {
    width: "20%",
    minWidth: "150px"
  },
  fieldMax:{
    width: "90%"
  },
  sendButton: {
    marginTop: "20px",
    backgroundColor: "#263238",
    color: "#eceff1",
    width: "100px"
  }
});

export default useStyles;
