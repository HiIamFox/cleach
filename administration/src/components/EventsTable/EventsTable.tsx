/* React components */
import React, { ReactElement, useState } from "react";
import Panel from "../../containers/Panel";
import TableError from "../TableError";
/* React components */

/* GraphQL client */
import { useQuery, useMutation } from "@apollo/react-hooks";
import { GET_EVENTS } from "../../graphql/queries";
/* GraphQL client */

/* Material UI */
import {
  TableRow,
  TableCell,
  Paper,
  Table,
  TableHead,
  TableBody,
  IconButton,
  Chip,
  TableFooter
} from "@material-ui/core";
import Skeleton from "@material-ui/lab/Skeleton";
import {
  Delete,
  KeyboardArrowLeft,
  KeyboardArrowRight
} from "@material-ui/icons";
/* Material UI */

/* FontAwesome Icons */
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHorseHead,
  faPlaceOfWorship
} from "@fortawesome/free-solid-svg-icons";
/* FontAwesome Icons */

import { EventType } from "../../graphql/models/event";
import tableStyles from "../../styles/tableStyles";
import { REMOVE_EVENT } from "../../graphql/mutations";

const EventsTable: React.FC = () => {
  const classes = tableStyles();

  const { loading, error, data, refetch } = useQuery(GET_EVENTS, {
    variables: { offset: 0 }
  });
  const [removeEvent] = useMutation(REMOVE_EVENT);
  const [page, setPage] = useState<number>(0);

  const handleChangePage = (change: number) => {
    refetch({ offset: (page + change) * 5 });
    setPage(page + change);
  };
  const createTableSkeleton = () => {
    const rows: ReactElement[] = [];
    for (let i = 0; i < 5; i++) {
      rows.push(
        <TableRow key={i}>
          <TableCell align="left">
            <Skeleton variant="text" />
          </TableCell>
          <TableCell align="left">
            <Skeleton variant="text" />
          </TableCell>
          <TableCell align="left">
            <Skeleton variant="text" />
          </TableCell>
          <TableCell align="left">
            <Skeleton variant="text" />
          </TableCell>
          <TableCell align="right"></TableCell>
        </TableRow>
      );
    }
    return rows;
  };

  if (error) {
    return (
      <Panel
        title="Events table"
        content={<TableError error={error} refetch={refetch} />}
      />
    );
  }

  return (
    <Panel
      title="Events table"
      content={
        <Paper className={classes.table}>
          <Table size="small" aria-label="simple table">
            <TableHead>
              <TableRow className={classes.tableHeader}>
                <TableCell align="left">Title</TableCell>
                <TableCell align="left">Place</TableCell>
                <TableCell align="left">Start</TableCell>
                <TableCell align="left">End</TableCell>
                <TableCell align="center">Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {loading
                ? createTableSkeleton()
                : data.events.map((event: EventType) => (
                    <TableRow key={event.start.toString()}>
                      <TableCell align="left">{event.title}</TableCell>
                      <TableCell align="left">
                        <Chip
                          icon={
                            event.place === "Jízdárna" ? (
                              <FontAwesomeIcon icon={faHorseHead} />
                            ) : event.place === "Zámek" ? (
                              <FontAwesomeIcon icon={faPlaceOfWorship} />
                            ) : (
                              <></>
                            )
                          }
                          label={event.place}
                        />
                      </TableCell>
                      <TableCell align="left">{event.start}</TableCell>
                      <TableCell align="left">{event.end}</TableCell>
                      <TableCell align="center">
                        <>
                          <IconButton
                            size="small"
                            onClick={() => {
                              removeEvent({
                                variables: { uid: event.uid }
                              }).then(() => refetch());
                            }}
                          >
                            <Delete />
                          </IconButton>
                        </>
                      </TableCell>
                    </TableRow>
                  ))}
            </TableBody>
            <TableFooter>
              <TableRow>
                <TableCell />
                <TableCell />
                <TableCell />
                <TableCell />
                <TableCell align="right">
                  {loading ? (
                    <></>
                  ) : (
                    <>
                      <IconButton
                        disabled={page === 0}
                        aria-label="pageDown"
                        onClick={() => handleChangePage(-1)}
                      >
                        <KeyboardArrowLeft />
                      </IconButton>
                      <IconButton
                        disabled={data.events.length < 5}
                        aria-label="pageUp"
                        onClick={() => handleChangePage(1)}
                      >
                        <KeyboardArrowRight />
                      </IconButton>
                    </>
                  )}
                </TableCell>
              </TableRow>
            </TableFooter>
          </Table>
        </Paper>
      }
    />
  );
};

export default EventsTable;
