import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  errorText: {
    margin: "0 10% 20px 10%"
  },
  reloadButton: {
    backgroundColor: "#263238",
    color: "#eceff1",
    width: "100px"
  }
});

export default useStyles;
