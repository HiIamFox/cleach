/* React components */
import React from "react";
/* React components */

/* Material UI */
import { Typography, Button } from "@material-ui/core";
/* Material UI */

import { ApolloError } from "apollo-boost";

import useStyles from "./TableErrorStyles";

interface TableErrorProps {
  error: ApolloError;
  refetch: () => void;
}

const TableError: React.FC<TableErrorProps> = ({
  error,
  refetch
}: TableErrorProps) => {
  const classes = useStyles();

  return (
    <>
      <div className={classes.errorText}>
        <Typography variant="h5" align="center">
          {error.name}
        </Typography>
        <Typography variant="body1" align="center">
          {error.message}
        </Typography>
      </div>
      <Button
        className={classes.reloadButton}
        variant="contained"
        size="medium"
        onClick={() => {
          refetch();
        }}
      >
        Reload
      </Button>
    </>
  );
};

export default TableError;
