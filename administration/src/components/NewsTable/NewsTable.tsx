import React, { ReactElement, useState } from "react";
import Panel from "../../containers/Panel";
import {
  Paper,
  Table,
  TableHead,
  TableBody,
  TableCell,
  TableRow,
  TableFooter,
  IconButton
} from "@material-ui/core";
import tableStyles from "../../styles/tableStyles";
import Skeleton from "@material-ui/lab/Skeleton";
import { useQuery } from "@apollo/react-hooks";
import { GET_NEWS } from "../../graphql/queries";
import { NewsType } from "../../graphql/models/news";
import {
  KeyboardArrowLeft,
  KeyboardArrowRight
} from "@material-ui/icons";
import TableError from "../TableError";

const NewsTable: React.FC = () => {
  const classes = tableStyles();
  const [page, setPage] = useState<number>(0);

  const handleChangePage = (change: number) => {
    refetch({ offset: (page + change) * 5 });
    setPage(page + change);
  };
  const { loading, error, data, refetch } = useQuery(GET_NEWS);

  const createTableSkeleton = () => {
    const rows: ReactElement[] = [];
    for (let i = 0; i < 5; i++) {
      rows.push(
        <TableRow key={i}>
          <TableCell align="left">
            <Skeleton variant="text" />
          </TableCell>
          <TableCell align="left">
            <Skeleton variant="text" />
          </TableCell>
          <TableCell align="left">
            <Skeleton variant="text" />
          </TableCell>
          <TableCell align="right"></TableCell>
        </TableRow>
      );
    }
    return rows;
  };

  if (error) {
    return (
      <Panel
        title="Events table"
        content={<TableError error={error} refetch={refetch} />}
      />
    );
  }

  return (
    <Panel
      title="News table"
      content={
        <Paper className={classes.table}>
          <Table size="small" aria-label="simple table">
            <TableHead>
              <TableRow className={classes.tableHeader}>
                <TableCell align="left">Title</TableCell>
                <TableCell align="left">Created at</TableCell>
                <TableCell align="left">Detail</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {loading
                ? createTableSkeleton()
                : data.news.map((news: NewsType) => (
                    <TableRow key={news.uid}>
                      <TableCell align="left">{news.title}</TableCell>
                      <TableCell align="left">{news.createdAt}</TableCell>
                      <TableCell align="left">
                        {news.detail.substring(0, 80)}...
                      </TableCell>
                    </TableRow>
                  ))}
            </TableBody>
            <TableFooter>
              <TableRow>
                <TableCell />
                <TableCell />
                <TableCell align="right">
                  {loading ? (
                    <></>
                  ) : (
                    <>
                      <IconButton
                        disabled={page === 0}
                        aria-label="pageDown"
                        onClick={() => handleChangePage(-1)}
                      >
                        <KeyboardArrowLeft />
                      </IconButton>
                      <IconButton
                        disabled={data.news.length < 5}
                        aria-label="pageUp"
                        onClick={() => handleChangePage(1)}
                      >
                        <KeyboardArrowRight />
                      </IconButton>
                    </>
                  )}
                </TableCell>
              </TableRow>
            </TableFooter>
          </Table>
        </Paper>
      }
    />
  );
};

export default NewsTable;
