import React from "react";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import Login from "./pages/Login";
//import Dashboard from "./pages/Dashboard";
import Users from "./pages/Users";
import AuthedRoute from "./auth/AuthedRoute";
import { AuthProvider } from "./auth/AuthContext";
import Events from "./pages/Events";
import client from "./graphql/client";
import { ApolloProvider } from "@apollo/react-hooks";
import ThemeContainer from "./styles/ThemeProvider";
import NewsAndNotification from "./pages/NewsAndNotification";

const App: React.FC = () => {
  return (
    <AuthProvider>
      <ApolloProvider client={client}>
        <ThemeContainer>
          <Router>
            <Route exact path="/" render={() => <Redirect to="/events" />} />
            <Route exact path="/login" component={Login} />
            {/* <AuthedRoute exact path="/dashboard" component={Dashboard} /> */}
            <AuthedRoute exact path="/users" component={Users} />
            <AuthedRoute exact path="/events" component={Events} />
            <AuthedRoute exact path="/news" component={NewsAndNotification} />
          </Router>
        </ThemeContainer>
      </ApolloProvider>
    </AuthProvider>
  );
};

export default App;
