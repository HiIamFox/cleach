import React, { useEffect, useState, createContext } from "react";
import app from "../firebase";
import { setStoredToken, removeStoredToken } from "./LocalStorage";

const defaultState = localStorage.getItem("token");

export const AuthContext = createContext<string | null>(defaultState);

export const AuthProvider: React.FC = ({ children }) => {
  const [idToken, setIdToken] = useState<string | null>(defaultState);
  useEffect(() => {
    app.auth().onAuthStateChanged(async user => {
      if (user) {
        const role = (await user.getIdTokenResult()).claims.role;
        if (role === "ADMIN" || role === "MODERATOR") {
          user.getIdToken(true).then(token => {
            setStoredToken(token);
            setIdToken(token);
          });
        }else{
          alert("Na toto nemáš oprávnění");
        }
      } else {
        removeStoredToken();
        setIdToken(null);
      }
    });
  }, []);

  return (
    <AuthContext.Provider value={idToken}>{children}</AuthContext.Provider>
  );
};
