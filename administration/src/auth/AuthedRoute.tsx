import React, { useContext, createElement } from "react";
import { Route, Redirect, RouteProps } from "react-router-dom";
import { AuthContext } from "./AuthContext";

/**
 * Creates private route. Only accessible by logged user.
 *
 * @param {any} { component, ...rest }
 * @returns React.FunctionComponent
 */
const AuthedRoute: React.FC<RouteProps> = ({ component, ...rest }: any) => {
  const idToken = useContext(AuthContext);
  const routeComponent = (props:any) => (
    !!idToken
      ? createElement(component, props)
      : <Redirect to="/login" />
  );
  return <Route {...rest} render={routeComponent}/>
}

export default AuthedRoute;
