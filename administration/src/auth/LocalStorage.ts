/**
 * Set token key/value in local storage
 *
 * @param {string} token
 */
export const setStoredToken = (token: string) => {
  localStorage.setItem("token", token);
};

/**
 * Removes token from local storage
 *
 */
export const removeStoredToken = () => {
  localStorage.removeItem("token");
};
