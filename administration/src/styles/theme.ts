import { createMuiTheme, Theme } from "@material-ui/core";

const getTheme = (): Theme => {

  const mainTheme = createMuiTheme({
    palette: {
      type: false ? "dark" : "light",
      primary: {
        main: "#263238",
        light: "#747C80"
      },
      secondary: {
        main: "#EBECEC",
        dark: "#C3C7C8"
      }
    }
  });

  return mainTheme;
};

export default getTheme;
