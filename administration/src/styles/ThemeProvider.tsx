import React from "react";
import { ThemeProvider } from "@material-ui/styles";
import getTheme from "./theme";

const ThemeContainer: React.FC<{}> = ({ children }) => {

    return <>
        <ThemeProvider theme={getTheme()}>
            {children}
        </ThemeProvider>
    </>;
};

export default ThemeContainer;
