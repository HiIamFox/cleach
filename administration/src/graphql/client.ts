import { ApolloClient } from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import { createUploadLink } from "apollo-upload-client";

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: createUploadLink({
    uri: "https://us-central1-cleach-festival.cloudfunctions.net/graphql",
    headers: {
      authorization: `Bearer ${localStorage.getItem("token")}`
    }
  })
});

export default client;
