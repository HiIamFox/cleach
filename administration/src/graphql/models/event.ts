export interface EventType {
  uid: string;
  title: string;
  place: string;
  image: string;
  end: Date;
  start: Date;
  detail: string;
}
