export interface UserType {
  uid: string;
  email: string;
  displayName?: string;
  role: UserRole;
  disabled: boolean;
}

export enum UserRole {
  DEFAULT = "DEFAULT",
  MODERATOR = "MODERATOR",
  ADMIN = "ADMIN"
}
