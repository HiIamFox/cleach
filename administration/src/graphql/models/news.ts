export interface NewsType {
  uid: string;
  title: string;
  createdAt: string;
  detail: string;
}
