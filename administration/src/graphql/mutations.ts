import gql from "graphql-tag";

export const EDIT_USER = gql`
  mutation EditUser($editedUser: UserInput!) {
    editUser(editedUser: $editedUser) {
      displayName
      email
    }
  }
`;

export const SEND_NOTIFICATION = gql`
  mutation sendNotification($notification: NotificationInput!) {
    sendNotification(notification: $notification) {
      uid
    }
  }
`;

export const EDIT_ROLE = gql`
  mutation grantUserRole($email: String!, $role: String!) {
    grantUserRole(email: $email, role: $role) {
      role
    }
  }
`;

export const ADD_EVENT = gql`
  mutation addEvent($event: EventInput!) {
    addEvent(event: $event) {
      uid
    }
  }
`;

export const REMOVE_EVENT = gql`
  mutation removeEvent($uid: String!){
    removeEvent(uid: $uid)
  }
`;
