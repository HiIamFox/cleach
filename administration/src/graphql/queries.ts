import gql from "graphql-tag";

export const GET_USERS = gql`
  query getUsers {
    getUsers {
      users {
        uid
        role
        email
        displayName
        disabled
      }
    }
  }
`;

export const GET_EVENTS = gql`
  query getEvents($offset: Float) {
    events(offset: $offset) {
      uid
      title
      place
      end
      start
    }
  }
`;

export const GET_NEWS = gql`
  query getNews {
    news {
      uid
      title
      createdAt
      detail
    }
  }
`;
