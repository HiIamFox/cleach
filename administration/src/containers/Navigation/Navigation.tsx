import React from "react";
import clsx from "clsx";
import {
  IconButton,
  AppBar,
  useTheme,
  Toolbar,
  Divider,
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Button
} from "@material-ui/core";
import { useStyles } from "./navigationStyles";
import {
  ChevronLeft,
  ChevronRight,
  Menu,
  //Dashboard,
  Person,
  Event,
  Announcement
} from "@material-ui/icons";
import { withRouter } from "react-router-dom";
import app from "../../firebase";

const Navigation: React.FC = ({ children, history }: any) => {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const signOut = () => {
    app.auth().signOut();
  };

  return (
    <div className={classes.navigation}>
      <AppBar
        color="primary"
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, open && classes.hide)}
          >
            <Menu color="primary" />
          </IconButton>
          <span className={classes.gap} />
          <img
            src="https://cleach.cz/wp-content/uploads/2019/08/cleach_logo.png"
            alt="Cleach festival logo"
            className={classes.logo}
          ></img>
          <span className={classes.gap} />
          <Button color="primary" onClick={() => signOut()}>
            Logout
          </Button>
        </Toolbar>
      </AppBar>
      <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        open={open}
        classes={{
          paper: classes.drawerPaper
        }}
      >
        <div className={classes.drawerHeader}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "ltr" ? (
              <ChevronLeft color="secondary" />
            ) : (
              <ChevronRight color="secondary" />
            )}
          </IconButton>
        </div>
        <Divider light={true} />
        <List>
{/*           <ListItem
            button
            key="Dashboard"
            onClick={() => history.push("/dashboard")}
          >
            <ListItemIcon>
              <Dashboard style={{ color: "#eceff1" }} />
            </ListItemIcon>
            <ListItemText primary="Dashboard" />
          </ListItem> */}
          <ListItem button key="Events" onClick={() => history.push("/events")}>
            <ListItemIcon>
              <Event style={{ color: "#eceff1" }} />
            </ListItemIcon>
            <ListItemText primary="Events" />
          </ListItem>
          <ListItem button key="Users" onClick={() => history.push("/users")}>
            <ListItemIcon>
              <Person style={{ color: "#eceff1" }} />
            </ListItemIcon>
            <ListItemText primary="Users" />
          </ListItem>
          <ListItem button key="News" onClick={() => history.push("/news")}>
            <ListItemIcon>
              <Announcement style={{ color: "#eceff1" }} />
            </ListItemIcon>
            <ListItemText primary="News and notifications" />
          </ListItem>
        </List>
      </Drawer>
      <main
        className={clsx(classes.content, {
          [classes.contentShift]: open
        })}
      >
        <div className={classes.drawerHeader} />
        {children}
      </main>
    </div>
  );
};

export default withRouter(Navigation);
