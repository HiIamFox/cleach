import { makeStyles, Theme } from "@material-ui/core/styles";
import getTheme from "../../styles/theme";

const useStyles = makeStyles((theme: Theme = getTheme()) => ({
  panel: {
    padding: "15px 20px 20px 20px",
    marginBottom: "25px",
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "center",
    minHeight: "200px",
    backgroundColor: theme.palette.secondary.main
  },
  panelHeader: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyItems: "space-between",
    height: "60px",
    color: theme.palette.primary.main
  },
  panelContent: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    flexGrow: 1,
    alignItems: "center"
  }
}));

export default useStyles;
