import React, { ReactElement } from "react";
import { Paper, Typography } from "@material-ui/core";
import useStyles from "./panelStyles";

interface PanelProps {
  title: String;
  panelMenu?: ReactElement;
  content: ReactElement;
}

const Panel: React.FC<PanelProps> = ({
  title,
  panelMenu,
  content
}: PanelProps) => {
  const classes = useStyles();

  return (
    <Paper className={classes.panel}>
      <div className={classes.panelHeader}>
        <Typography variant="h6" align="right">{title}</Typography>
        {panelMenu}
      </div>
      <div className={classes.panelContent}>{content}</div>
    </Paper>
  );
};

export default Panel;
