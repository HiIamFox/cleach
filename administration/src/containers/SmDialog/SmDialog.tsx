import React from "react";

/* Material design */
import { Dialog, DialogTitle } from "@material-ui/core";
/* Material desing */

interface SmDialogProps {
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  title: string;
}

const SmDialog: React.FC<SmDialogProps> = ({
  open,
  setOpen,
  title,
  children
}) => {
  const handleClose = () => setOpen(false);

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="smdialog-title"
      fullWidth={true}
      maxWidth="sm"
    >
      <DialogTitle id="smdialog-title">{title}</DialogTitle>
      {children}
    </Dialog>
  );
};

export default SmDialog;
