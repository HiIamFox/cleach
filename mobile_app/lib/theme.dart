import 'package:flutter/material.dart';

final defaultTheme = ThemeData(
  backgroundColor: Color.fromRGBO(38, 50, 56, 1),
  primaryTextTheme: TextTheme(
    headline: TextStyle(
      color: Color.fromRGBO(38, 50, 56, 1),
      fontSize: 20,
    ),
    overline: TextStyle(
      color: Colors.orange,
      fontSize: 20,
    ),
    body1: TextStyle(
      fontSize: 15,
      color: Color.fromRGBO(38, 50, 56, 1),
    ),
    title: TextStyle(
      fontSize: 25,
      fontWeight: FontWeight.bold,
      color: Color.fromRGBO(38, 50, 56, 1),
    ),
    subtitle: TextStyle(
      fontSize: 15,
      fontWeight: FontWeight.bold,
      color: Color.fromRGBO(38, 50, 56, 1),
    ),
    display1: TextStyle(
      fontSize: 18,
      fontWeight: FontWeight.bold,
      color: Color.fromRGBO(38, 50, 56, 1),
    ),
  ),
  textTheme: TextTheme(
    headline: TextStyle(
      fontSize: 20,
      color: Colors.white,
    ),
    body1: TextStyle(
      fontSize: 15,
      color: Colors.white,
    ),
  ),
  iconTheme: IconThemeData(
    size: 30,
    color: Colors.white,
  ),
  highlightColor: Colors.transparent,
  splashColor: Colors.transparent,
);
