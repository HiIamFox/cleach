class EventListType {
  String uid;
  String title;
  String place;
  DateTime end;
  DateTime start;

  EventListType.fromJson(Map<String, dynamic> json)
      : uid = json['uid'],
        title = json['title'],
        place = json['place'],
        start = DateTime.parse(json['start']),
        end = DateTime.parse(json['end']);
}

class EventDetailType {
  String title;
  String place;
  String image;
  String detail;
  DateTime end;
  DateTime start;

  EventDetailType.fromJson(Map<String, dynamic> json)
      : title = json['title'],
        place = json['place'],
        image = json['image'],
        detail = json['detail'],
        start = DateTime.parse(json['start']),
        end = DateTime.parse(json['end']);
}
