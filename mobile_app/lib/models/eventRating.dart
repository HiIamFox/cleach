class EventRatingType {
  String uid;
  String displayName;
  String eventUid;
  int value;
  String comment;
  DateTime createdAt;

  EventRatingType.fromJson(Map<String, dynamic> json)
      : displayName = json["displayName"],
        eventUid = json["eventUid"],
        value = json["value"],
        comment = json["comment"],
        createdAt = DateTime.parse(json['createdAt']);
}
