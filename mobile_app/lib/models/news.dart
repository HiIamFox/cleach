class NewsType {
  String title;
  String image;
  String detail;

  NewsType.fromJson(Map<String, dynamic> json)
      : title = json['title'],
        image = json['image'],
        detail = json['detail'];
}

class NotificationType {
  String title;
  String detail;

  NotificationType.fromJson(Map<String, dynamic> json)
      : title = json['title'],
        detail = json['detail'];
}
