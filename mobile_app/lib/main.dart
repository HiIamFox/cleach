import 'package:firebase_messaging/firebase_messaging.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:mobile_app/ui/pages/defaultPage.dart';
import 'theme.dart';
import 'package:mobile_app/services/secureStorage.dart';
import 'package:mobile_app/ui/components/dialogs/notificationDialog.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final SecureStorage storage = new SecureStorage();

  final HttpLink httpLink = HttpLink(
      uri: 'https://us-central1-cleach-festival.cloudfunctions.net/graphql',
      headers: {"authorization": 'Bearer ${await storage.getToken()}'});
  ValueNotifier<GraphQLClient> client = ValueNotifier(
    GraphQLClient(
      cache: InMemoryCache(),
      link: httpLink,
    ),
  );
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) => runApp(
            GraphQLProvider(
              client: client,
              child: MyApp(),
            ),
          ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Studentská jízda',
      theme: defaultTheme,
      home: MessageHandler(),
    );
  }
}

class MessageHandler extends StatefulWidget {
  @override
  _MessageHandlerState createState() => _MessageHandlerState();
}

class _MessageHandlerState extends State<MessageHandler> {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  int initialIndex = 0;

  @override
  void initState() {
    super.initState();
    _firebaseMessaging.subscribeToTopic("important");
    _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
          showDialog(
      context: context,
      builder: (context) => NotificationDialog(message),
    );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: DefaultPage(),
    );
  }
}
