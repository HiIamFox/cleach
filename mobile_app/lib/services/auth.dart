import 'package:firebase_auth/firebase_auth.dart';
import 'package:mobile_app/ui/components/errorToast.dart';
import './secureStorage.dart';
import 'package:mobile_app/models/user.dart';

class Auth {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final SecureStorage _storage = new SecureStorage();

  Future<bool> isLogged() async =>
      await _auth.currentUser() == null ? false : true;

  Future<String> signInWithEmail(String email, String password) async {
    AuthResult _result;
    try {
      _result = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
    } catch (error) {
      return error.code;
    }
    _storeToken(await _result.user.getIdToken());
    return null;
  }

  Future<void> registerByEmail(
      String email, String password, String displayName) async {
    try {
      await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
    } catch (error) {
      String message;
      if (error.code == "ERROR_WEAK_PASSWORD")
        message = "Příliš slabé heslo";
      else if (error.code == "ERROR_INVALID_EMAIL")
        message = "Neplatná emailová adresa.";
      else
        message = "Email je již použit.";
      ErrorToast.showToast(message);
    }
    if (await this.isLogged()) {
      UserUpdateInfo updatedInfo = UserUpdateInfo();
      updatedInfo.displayName = displayName;
      await (await _auth.currentUser()).updateProfile(updatedInfo);
    }
  }

  Future<User> getUserInfo() async {
    User user = new User();
    FirebaseUser firebaseUser = await _auth.currentUser();
    user.uid = firebaseUser.uid;
    user.displayName = firebaseUser.displayName;
    user.email = firebaseUser.email;
    return user;
  }

  Future<void> signOut() async {
    await _auth.signOut();
  }

  Future<void> refreshToken() async {
    if (await isLogged()) {
      _storeToken(await (await _auth.currentUser()).getIdToken());
    }
  }

  void _storeToken(IdTokenResult tokenResult) {
    _storage.setToken(tokenResult.token);
  }
}
