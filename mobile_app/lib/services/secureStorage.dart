import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class SecureStorage {
  final storage = new FlutterSecureStorage();
  SecureStorage._privateConstructor();

  static final SecureStorage _instance = SecureStorage._privateConstructor();

  factory SecureStorage() {
    return _instance;
  }

  void setToken(String token) {
    storage.write(key: "token", value: token);
  }

  Future<String> getToken() {
    return storage.read(key: "token");
  }

  void setEmail(String email) {
    storage.write(key: "email", value: email);
  }

  Future<String> getEmail() async {
    String email = await storage.read(key: "email");
    return email == null ? '' : email;
  }
}
