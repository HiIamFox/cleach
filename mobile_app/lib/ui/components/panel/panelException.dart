import 'package:flutter/material.dart';

class PanelException extends StatelessWidget {
  final VoidCallback refetch;
  PanelException(this.refetch);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Text(
          "Nastala chyba. Prosím opakujte akci.",
          style: Theme.of(context).primaryTextTheme.body1,
        ),
        RaisedButton(
          color: Theme.of(context).backgroundColor,
          child: Text(
            "Zkusit znovu",
            style: TextStyle(color: Colors.white),
          ),
          onPressed: () => refetch(),
        ),
      ],
    );
  }
}
