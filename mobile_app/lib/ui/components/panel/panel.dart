import 'package:flutter/material.dart';

class Panel extends StatelessWidget {
  final Widget child;
  Panel(this.child);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.vertical(
        top: Radius.circular(35),
      ),
      child: Container(
        width: MediaQuery.of(context).size.width,
        color: Color.fromRGBO(250, 250, 250, 1),
        child: child,
      ),
    );
  }
}
