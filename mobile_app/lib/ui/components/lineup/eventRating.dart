import 'package:flutter/material.dart';
import 'package:mobile_app/models/eventRating.dart';

class EventRating extends StatelessWidget {
  final EventRatingType eventRating;
  EventRating(this.eventRating);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(eventRating.displayName == null ? "" : eventRating.displayName,
              style: Theme.of(context).primaryTextTheme.subtitle),
          Padding(
            padding: EdgeInsets.only(top: 5),
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.star,
                  color:
                      eventRating.value >= 1 ? Colors.yellow : Colors.black38,
                  size: 20,
                ),
                Icon(
                  Icons.star,
                  color:
                      eventRating.value >= 2 ? Colors.yellow : Colors.black38,
                  size: 20,
                ),
                Icon(
                  Icons.star,
                  color:
                      eventRating.value >= 3 ? Colors.yellow : Colors.black38,
                  size: 20,
                ),
                Icon(
                  Icons.star,
                  color:
                      eventRating.value >= 4 ? Colors.yellow : Colors.black38,
                  size: 20,
                ),
                Icon(
                  Icons.star,
                  color:
                      eventRating.value == 5 ? Colors.yellow : Colors.black38,
                  size: 20,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 5),
                  child: Text(
                    "${this.eventRating.createdAt.day}.${this.eventRating.createdAt.month}.${this.eventRating.createdAt.year}",
                    style: Theme.of(context).primaryTextTheme.body1,
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 10),
            child: Text(
              eventRating.comment,
              style: Theme.of(context).primaryTextTheme.body1,
              textAlign: TextAlign.left,
            ),
          ),
          Divider(),
        ],
      ),
    );
  }
}
