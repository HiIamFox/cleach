import 'package:flutter/material.dart';

class DayMenu extends StatefulWidget {
  final void Function(int) callback;
  final int selectedIndex;
  DayMenu({Key key, @required this.selectedIndex, @required this.callback})
      : super(key: key);

  @override
  _DayMenuState createState() => _DayMenuState();
}

class _DayMenuState extends State<DayMenu> with SingleTickerProviderStateMixin {
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = TabController(
        initialIndex: widget.selectedIndex, vsync: this, length: 3);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width*0.7,
      height: 30,
      child: TabBar(
        controller: _tabController,
        tabs: [
          Text("ČT"),
          Text("PÁ"),
          Text("SO"),
        ],
        labelColor: Colors.white,
        labelStyle: TextStyle(fontSize: 24),
        unselectedLabelColor: Color.fromRGBO(199, 199, 199, 1),
        unselectedLabelStyle: TextStyle(fontSize: 18),
        indicator: BoxDecoration(),
        labelPadding: EdgeInsets.only(left: 0, right: 0),
        onTap: (index) => widget.callback(index),
      ),
    );
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }
}
