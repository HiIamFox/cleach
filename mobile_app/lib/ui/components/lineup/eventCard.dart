import 'package:flutter/material.dart';
import 'package:mobile_app/models/event.dart';
import 'package:mobile_app/ui/pages/eventDetailPage.dart';

class EventCard extends StatelessWidget {
  final EventListType event;
  EventCard(this.event);

  final gradientOne = LinearGradient(
    colors: [Color.fromRGBO(220, 20, 20, 1), Color.fromRGBO(247, 143, 63, 1)],
  );
  final gradientTwo = LinearGradient(
    colors: [Color.fromRGBO(25, 46, 187, 1), Color.fromRGBO(52, 182, 214, 1)],
  );
  final gradientThree = LinearGradient(
    colors: [Color.fromRGBO(180, 31, 225, 1), Color.fromRGBO(189, 8, 146, 1)],
  );

  void openDetailPage(BuildContext context, String uid) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => EventDetailPage(uid)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        child: GestureDetector(
          onTap: () => openDetailPage(context, event.uid),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.85,
            height: 100,
            decoration: BoxDecoration(
              gradient: event.place == "Zámek"
                  ? gradientOne
                  : event.place == "Jízdárna" ? gradientTwo : gradientThree,
            ),
            child: Padding(
              padding: EdgeInsets.fromLTRB(15, 8, 15, 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    event.title,
                    style: Theme.of(context).textTheme.headline,
                  ),
                  Expanded(flex: 1, child: Container()),
                  Text(
                      "${event.start.hour < 10 ? "0" : ""}${event.start.hour}:${event.start.minute < 10 ? "0" : ""}${event.start.minute} - ${event.end.hour < 10 ? "0" : ""}${event.end.hour}:${event.end.minute < 10 ? "0" : ""}${event.end.minute}"),
                  Text(event.place),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
