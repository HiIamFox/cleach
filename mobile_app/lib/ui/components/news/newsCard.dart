import 'package:flutter/material.dart';
import 'package:mobile_app/models/news.dart';

class NewsCard extends StatelessWidget {
  final NewsType news;
  NewsCard(this.news);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        child: GestureDetector(
          onTap: () => null,
          child: Container(
            width: MediaQuery.of(context).size.width * 0.85,
            child: Padding(
              padding: EdgeInsets.fromLTRB(15, 8, 15, 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    news.title,
                    style: Theme.of(context).primaryTextTheme.headline,
                  ),
                  news.image == null
                      ? null
                      : Image.network(
                          news.image,
                          height: 200,
                        ),
                  Text(
                    news.detail,
                    style: Theme.of(context).primaryTextTheme.body1,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
