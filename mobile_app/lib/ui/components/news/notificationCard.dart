import 'package:flutter/material.dart';
import 'package:mobile_app/models/news.dart';

class NotificationCard extends StatelessWidget {
  final NotificationType notification;
  NotificationCard(this.notification);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        child: GestureDetector(
          onTap: () => null,
          child: Container(
            width: MediaQuery.of(context).size.width * 0.85,
            child: Padding(
              padding: EdgeInsets.fromLTRB(15, 8, 15, 15),
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(right: 15),
                    child: Icon(
                      Icons.warning,
                      color: Colors.orange,
                    ),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(bottom: 5),
                          child: Text(
                            notification.title,
                            style: Theme.of(context).primaryTextTheme.overline,
                          ),
                        ),
                        Text(
                          notification.detail,
                          softWrap: true,
                          style: Theme.of(context).primaryTextTheme.body1,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
