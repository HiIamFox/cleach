import 'package:flutter/material.dart';

class ReturnButton extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(20),
      child: Container(
        height: 35,
        width: 35,
        decoration: ShapeDecoration(
          color: Colors.white,
          shape: CircleBorder(),
          shadows: <BoxShadow>[
            BoxShadow(
              color: Colors.black54,
              blurRadius: 3.0,
              spreadRadius: 1.0,
              offset: Offset(
                0.5,
                2.0,
              ),
            ),
          ],
        ),
        child: IconButton(
          iconSize: 18,
          tooltip: "Zpět",
          icon: Icon(Icons.arrow_back),
          color: Color.fromRGBO(38, 50, 56, 1),
          onPressed: () => Navigator.pop(context),
        ),
      ),
    );
  }
}
