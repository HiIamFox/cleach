import 'package:flutter/material.dart';
import 'package:mobile_app/ui/components/errorToast.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:mobile_app/services/auth.dart';

class _RatingData {
  int value = 0;
  String comment;
}

class AddEventRatingDialog extends StatefulWidget {
  final String eventUid;
  AddEventRatingDialog(this.eventUid);

  @override
  _AddEventRatingDialogState createState() => _AddEventRatingDialogState();
}

class _AddEventRatingDialogState extends State<AddEventRatingDialog> {
  final String addRating = """
    mutation addRating(\$newRating: RatingInput!){
      addRating(
        newRating: \$newRating
      ) {
        comment
      }
    }
  """;
  final Auth _auth = new Auth();
  final _ratingFormKey = GlobalKey<FormState>();
  final _RatingData _ratingData = new _RatingData();
  final List<IconButton> _stars = List<IconButton>();

  void _generateStars() {
    _stars.clear();
    for (int i = 0; i < 5; i++) {
      _stars.add(
        IconButton(
          onPressed: () => this._starOnPressed(i),
          color: _ratingData.value >= i + 1 ? Colors.yellow : Colors.black38,
          icon: Icon(
            Icons.star,
          ),
        ),
      );
    }
  }

  void _starOnPressed(int index) {
    setState(() {
      this._ratingData.value = index + 1;
      this._generateStars();
    });
  }

  @override
  Widget build(BuildContext context) {
    this._generateStars();
    return SimpleDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(15),
        ),
      ),
      title: Center(
        child: Text("Přidat hodnocení interpreta"),
      ),
      children: <Widget>[
        Mutation(
            options: MutationOptions(
              documentNode: gql(addRating),
              onCompleted: (dynamic resultData){
                Navigator.pop(context);
              },
            ),
            builder: (
              RunMutation runMutation,
              QueryResult result,
            ) {
              return Form(
                key: _ratingFormKey,
                child: Padding(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(bottom: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: _stars,
                        ),
                      ),
                      TextFormField(
                        maxLength: 300,
                        maxLines: null,
                        decoration: InputDecoration(
                          hintText: "Zde můžete vložit svůj komentář",
                          border: OutlineInputBorder(),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Color.fromRGBO(38, 50, 56, 1),
                              width: 1.5,
                            ),
                          ),
                        ),
                        onSaved: (comment) => _ratingData.comment = comment,
                      ),
                      RaisedButton(
                        textColor: Colors.white,
                        color: Theme.of(context).backgroundColor,
                        onPressed: () async {
                          if (_ratingFormKey.currentState.validate()) {
                            _ratingFormKey.currentState.save();
                            runMutation({
                              "newRating": {
                                "userUid": (await _auth.getUserInfo()).uid,
                                "eventUid": widget.eventUid,
                                "value": this._ratingData.value,
                                "comment": this._ratingData.comment
                              }
                            });
                            if(result.hasException){
                              ErrorToast.showToast("Nastala chyba při odesílání hodnocení. Prosím zkuste to později.");
                               
                            }
                          }
                        },
                        child: Text("Odeslat"),
                      ),
                    ],
                  ),
                ),
              );
            }),
      ],
    );
  }
}
