import 'package:flutter/material.dart';

class NotificationDialog extends StatelessWidget {
  final Map<String, dynamic> message;
  const NotificationDialog(this.message);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(15),
          ),
        ),
        title: Text(message['notification']['title']),
        content: Text(message['notification']['body']),
        actions: <Widget>[
          RaisedButton(
            color: Theme.of(context).backgroundColor,
            child: Text(
              "Ok",
              style: TextStyle(color: Colors.white),
            ),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ],
      );
  }
}