import 'package:flutter/material.dart';
import 'package:mobile_app/services/auth.dart';

class _RegisterData {
  String email;
  String password;
  String displayName;
}

class RegisterUserDialog extends StatelessWidget {
  final _registerFormKey = GlobalKey<FormState>();
  final Auth _auth = new Auth();
  final _RegisterData _registerData = _RegisterData();

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(15),
        ),
      ),
      title: Center(
        child: Text("Registrace"),
      ),
      children: <Widget>[
        Form(
          key: _registerFormKey,
          child: Padding(
            padding: EdgeInsets.only(left: 15, right: 15, top: 5, bottom: 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                TextFormField(
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(borderSide: BorderSide.none),
                    icon: Icon(
                      Icons.email,
                      color: Color.fromRGBO(38, 50, 56, 1),
                    ),
                    hintText: 'Email',
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Prosím vyplňte svoji emailovou adresu';
                    }
                    return null;
                  },
                  onSaved: (email) {
                    _registerData.email = email;
                  },
                ),
                Divider(),
                TextFormField(
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(borderSide: BorderSide.none),
                    icon: Icon(
                      Icons.person,
                      color: Color.fromRGBO(38, 50, 56, 1),
                    ),
                    hintText: 'Přezdívka',
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Prosím vyplňte svoji přezdívku';
                    }
                    return null;
                  },
                  onSaved: (displayName) {
                    _registerData.displayName = displayName;
                  },
                ),
                Divider(),
                TextFormField(
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(borderSide: BorderSide.none),
                    icon: Icon(
                      Icons.lock,
                      color: Color.fromRGBO(38, 50, 56, 1),
                    ),
                    hintText: 'Heslo',
                  ),
                  obscureText: true,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Prosím vyplňte svoje heslo';
                    }
                    return null;
                  },
                  onSaved: (password) {
                    _registerData.password = password;
                  },
                ),
                RaisedButton(
                  textColor: Colors.white,
                  color: Theme.of(context).backgroundColor,
                  onPressed: () async {
                    if (_registerFormKey.currentState.validate()) {
                      _registerFormKey.currentState.save();
                      await _auth.registerByEmail(_registerData.email,
                          _registerData.password, _registerData.displayName);
                      if (await _auth.isLogged()) Navigator.pop(context);
                    }
                  },
                  child: Text("Registrovat"),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
