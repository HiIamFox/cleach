import 'package:flutter/material.dart';
import 'package:mobile_app/services/auth.dart';
import 'package:mobile_app/models/user.dart';

class UserSettingsDialog extends StatelessWidget {
  final Auth _auth = new Auth();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _auth.getUserInfo(),
        builder: (BuildContext context, AsyncSnapshot<User> snapshot) {
          if (snapshot.hasError) {
            return SimpleDialog(
              title: Text("ERROR"),
            );
          } else if (snapshot.hasData) {
            return SimpleDialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(15),
                ),
              ),
              title: Text("Uživatelské informace",
                  style: Theme.of(context).primaryTextTheme.title),
              titlePadding: EdgeInsets.all(10),
              contentPadding: EdgeInsets.all(15),
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text(
                      "Jméno: ",
                      style: Theme.of(context).primaryTextTheme.display1,
                    ),
                    Text(
                      snapshot.data.displayName == null
                          ? ""
                          : snapshot.data.displayName,
                      style: Theme.of(context).primaryTextTheme.body1,
                    ),
                  ],
                ),
                Divider(),
                Row(
                  children: <Widget>[
                    Text(
                      "Email: ",
                      style: Theme.of(context).primaryTextTheme.display1,
                    ),
                    Text(
                      snapshot.data.email,
                      style: Theme.of(context).primaryTextTheme.body1,
                    ),
                  ],
                ),
                SizedBox(height: 20,),
                Center(
                  child: RaisedButton(
                    textColor: Colors.white,
                    color: Theme.of(context).backgroundColor,
                    onPressed: () {
                      _auth.signOut();
                      Navigator.pop(context);
                    },
                    child: Text('Odhlásit'),
                  ),
                ),
              ],
            );
          } else {
            return SimpleDialog(
              children: <Widget>[
                CircularProgressIndicator(),
              ],
            );
          }
        });
  }
}
