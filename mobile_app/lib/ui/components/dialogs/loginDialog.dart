import 'package:flutter/material.dart';
import 'package:mobile_app/services/auth.dart';
import 'package:mobile_app/ui/components/errorToast.dart';
import 'package:mobile_app/ui/components/dialogs/registerUserDialog.dart';
import 'package:mobile_app/services/secureStorage.dart';

class _LoginData {
  String email = '';
  String password = '';
}

class LoginDialog extends StatelessWidget {
  final _loginFormKey = GlobalKey<FormState>();
  final Auth _auth = new Auth();
  final _LoginData _loginData = new _LoginData();
  final SecureStorage _storage = new SecureStorage();

  String _handleLoginError(String errorCode) {
    if (errorCode == "ERROR_TOO_MANY_REQUESTS")
      return "Překročen limit přihlášení. Prosím vyčkejte chvíli.";
    else if (errorCode == "ERROR_USER_DISABLED")
      return "Účet zablokován. Kontaktujte podporu prosím.";
    else
      return "Uživatelské údaje se neshodují. Prosím zkontrolujte si je.";
  }

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(15),
        ),
      ),
      title: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.person,
              size: 40,
              color: Color.fromRGBO(38, 50, 56, 1),
            ),
            Text(
              "Přihlášení",
              style: Theme.of(context).primaryTextTheme.title,
            ),
          ]),
      children: <Widget>[
        Form(
          key: _loginFormKey,
          child: Padding(
            padding: EdgeInsets.only(left: 15, right: 15, top: 5, bottom: 5),
            child: FutureBuilder(
              future: _storage.getEmail(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.hasData) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      TextFormField(
                        initialValue:
                            snapshot.data == null ? "" : snapshot.data,
                        decoration: const InputDecoration(
                          border:
                              OutlineInputBorder(borderSide: BorderSide.none),
                          icon: Icon(
                            Icons.email,
                            color: Color.fromRGBO(38, 50, 56, 1),
                          ),
                          hintText: 'Email',
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Prosím vyplňte svoji emailovou adresu';
                          }
                          return null;
                        },
                        onSaved: (email) {
                          _loginData.email = email;
                        },
                      ),
                      Divider(),
                      TextFormField(
                        obscureText: true,
                        decoration: const InputDecoration(
                          border:
                              OutlineInputBorder(borderSide: BorderSide.none),
                          icon: Icon(
                            Icons.lock,
                            color: Colors.black,
                          ),
                          hintText: 'Heslo',
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Prosím zadejte svoje heslo';
                          }
                          return null;
                        },
                        onSaved: (password) {
                          _loginData.password = password;
                        },
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          RaisedButton(
                            textColor: Colors.white,
                            color: Theme.of(context).backgroundColor,
                            onPressed: () {
                              Navigator.of(context).pop();
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return RegisterUserDialog();
                                },
                              );
                            },
                            child: Text("Registrovat"),
                          ),
                          RaisedButton(
                            textColor: Colors.white,
                            color: Theme.of(context).backgroundColor,
                            onPressed: () async {
                              if (_loginFormKey.currentState.validate()) {
                                _loginFormKey.currentState.save();
                                _storage.setEmail(_loginData.email);
                                String _result = await _auth.signInWithEmail(
                                    _loginData.email, _loginData.password);
                                if (_result == null)
                                  Navigator.pop(context);
                                else {
                                  ErrorToast.showToast(
                                      _handleLoginError(_result));
                                }
                              }
                            },
                            child: Text('Přihlásit'),
                          ),
                        ],
                      ),
                    ],
                  );
                } else if (snapshot.hasError) {
                  return Text("Chyba");
                } else {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
              },
            ),
          ),
        ),
      ],
    );
  }
}
