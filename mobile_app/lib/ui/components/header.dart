import 'package:flutter/material.dart';
import 'package:mobile_app/ui/components/dialogs/loginDialog.dart';
import 'package:mobile_app/ui/components/dialogs/userSettingsDialog.dart';
import 'package:mobile_app/services/auth.dart';

class Header extends StatelessWidget {
  final Auth _auth = new Auth();

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        IconButton(
            icon: Icon(
              Icons.person,
              size: Theme.of(context).iconTheme.size,
              color: Theme.of(context).iconTheme.color,
            ),
            onPressed: () async {
              bool isLogged = await _auth.isLogged();
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return isLogged ? UserSettingsDialog() : LoginDialog();
                },
              );
            }),
        Image.asset(
          'assets/logofull01.png',
          width: MediaQuery.of(context).size.width * 0.55,
        ),
        IconButton(
          icon: Icon(
            Icons.settings,
            size: Theme.of(context).iconTheme.size,
            color: Theme.of(context).backgroundColor,
          ),
          onPressed: () => {},
        ),
      ],
    );
  }
}
