import 'package:flutter/material.dart';

class Navigation extends StatefulWidget {
  final void Function(int) callback;
  final int selectedIndex;
  Navigation({Key key, @required this.selectedIndex, @required this.callback}): super(key: key);

  @override
  _NavigationState createState() => _NavigationState();
}

class _NavigationState extends State<Navigation>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  
  @override
  void initState() {
    super.initState();
    _tabController = TabController(initialIndex: widget.selectedIndex, vsync: this, length: 3);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30,
      child: TabBar(
      controller: _tabController,
      tabs: [
        Text("Lineup"),
        Text("Aktuality"),
        Text("Mapa"),
      ],
      labelColor: Colors.white,
      labelStyle: TextStyle(fontSize: 24),
      unselectedLabelColor: Color.fromRGBO(199, 199, 199, 1),
      unselectedLabelStyle: TextStyle(fontSize: 18),
      indicator: BoxDecoration(),
      labelPadding: EdgeInsets.only(left: 0, right: 0),
      onTap: (index) => widget.callback(index),
      ),
    );
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }
}
