import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mobile_app/ui/components/panel/panel.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapPage extends StatefulWidget {
  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  Completer<GoogleMapController> _controller = Completer();

  final CameraPosition _festivalPosition = CameraPosition(
    target: LatLng(49.873404, 16.313031),
    zoom: 17,
  );
  Set<Marker> _markers = {};
  @override
  Widget build(BuildContext context) {
    return Panel(
      GoogleMap(
        mapType: MapType.normal,
        initialCameraPosition: _festivalPosition,
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
          setState(() {
            _markers.add(
              Marker(
                markerId: MarkerId("jizdarna"),
                position: LatLng(49.873031, 16.314310),
              ),
            );
            _markers.add(
              Marker(
                markerId: MarkerId("el_lamino"),
                position: LatLng(49.873766, 16.314163),
              ),
            );
            _markers.add(
              Marker(
                markerId: MarkerId("zamek"),
                position: LatLng(49.873617, 16.312817),
              ),
            );
          });
        },
        markers: _markers,
      ),
    );
  }
}
