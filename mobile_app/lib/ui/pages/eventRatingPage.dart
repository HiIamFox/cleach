import 'package:flutter/material.dart';
import 'package:mobile_app/ui/components/errorToast.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:mobile_app/models/eventRating.dart';
import 'package:mobile_app/services/auth.dart';
import 'package:mobile_app/ui/components/dialogs/addEventRatingDialog.dart';
import 'package:mobile_app/ui/components/dialogs/loginDialog.dart';
import 'package:mobile_app/ui/components/lineup/eventRating.dart';
import 'package:mobile_app/ui/components/panel/panel.dart';
import 'package:mobile_app/ui/components/returnButton.dart';

class EventRatingPage extends StatelessWidget {
  final Auth _auth = new Auth();
  final String eventUid;
  EventRatingPage(this.eventUid);
  final String getRatings = """
    query getRatings(\$eventUid: String!){
      getRatings(eventUid: \$eventUid) {
        value
        comment
        displayName
        eventUid
        createdAt
      }
    }
  """;

  void openDialog(BuildContext context, VoidCallback refetch) async {
    if (await _auth.isLogged()) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AddEventRatingDialog(this.eventUid);
        },
      ).then((_) => refetch());
    } else {
      ErrorToast.showToast("Pro napsání hodnocení musíte být přihlášen/a ke svému účtu.");
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return LoginDialog();
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget child;
    return Query(
        options: QueryOptions(
            documentNode: gql(getRatings), variables: {"eventUid": eventUid}),
        builder: (QueryResult result,
            {VoidCallback refetch, FetchMore fetchMore}) {
          if (result.hasException) {
            child = Column(
              children: <Widget>[
                Text("Nastala chyba. Prosím opakujte akci."),
                RaisedButton(
                  onPressed: () => refetch(),
                  child: Text("Zkusit znovu"),
                ),
              ],
            );
          } else if (result.loading) {
            child = Center(
              child: CircularProgressIndicator(),
            );
          } else {
            final List<EventRatingType> ratings = List<EventRatingType>();
            final List<Widget> eventRatings = List<Widget>();
            eventRatings.add(SizedBox(
              height: 55,
            ));
            result.data['getRatings']
                .forEach((item) => ratings.add(EventRatingType.fromJson(item)));
            ratings.forEach(
                (eventRating) => eventRatings.add(EventRating(eventRating)));
            child = ListView(
              children: eventRatings,
            );
          }
          return Scaffold(
            floatingActionButton: Container(
              decoration: ShapeDecoration(
                color: Color.fromRGBO(38, 50, 56, 1),
                shape: CircleBorder(),
                shadows: <BoxShadow>[
                  BoxShadow(
                    color: Colors.black54,
                    blurRadius: 3.0,
                    spreadRadius: 1.0,
                    offset: Offset(
                      0.5,
                      2.0,
                    ),
                  ),
                ],
              ),
              child: IconButton(
                  color: Colors.white,
                  onPressed: () => openDialog(context, refetch),
                  icon: Icon(
                    Icons.create,
                  )),
            ),
            backgroundColor: Theme.of(context).backgroundColor,
            body: SafeArea(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Expanded(flex: 1, child: Container()),
                  Expanded(
                    flex: 15,
                    child: Stack(
                      children: <Widget>[
                        Panel(child),
                        ReturnButton(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
