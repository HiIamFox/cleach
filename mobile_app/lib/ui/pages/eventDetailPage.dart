import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:mobile_app/models/event.dart';
import 'package:mobile_app/ui/components/panel/panel.dart';
import 'package:mobile_app/ui/components/panel/panelException.dart';
import 'package:mobile_app/ui/components/returnButton.dart';
import 'package:mobile_app/ui/pages/eventRatingPage.dart';

class EventDetailPage extends StatelessWidget {
  final String uid;
  final String getEvent = """
    query getEvent(\$uid: String!){
      event(id: \$uid){
        title
        place
        image
        detail
        start
        end
      }
    }
  """;
  EventDetailPage(this.uid);

  @override
  Widget build(BuildContext context) {
    Widget child;
    return Scaffold(
      floatingActionButton: Container(
        decoration: ShapeDecoration(
          color: Color.fromRGBO(38, 50, 56, 1),
          shape: CircleBorder(),
          shadows: <BoxShadow>[
            BoxShadow(
              color: Colors.black54,
              blurRadius: 3.0,
              spreadRadius: 1.0,
              offset: Offset(
                0.5,
                2.0,
              ),
            ),
          ],
        ),
        child: IconButton(
          tooltip: "Hodnocení interpreta",
          icon: Icon(Icons.star),
          color: Colors.yellow,
          onPressed: () => Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => EventRatingPage(uid)),
          ),
        ),
      ),
      backgroundColor: Theme.of(context).backgroundColor,
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(flex: 1, child: Container()),
            Expanded(
              flex: 15,
              child: Stack(
                children: <Widget>[
                  Query(
                      options: QueryOptions(
                          documentNode: gql(getEvent), variables: {"uid": uid}),
                      builder: (QueryResult result,
                          {VoidCallback refetch, FetchMore fetchMore}) {
                        if (result.hasException) {
                          child = PanelException(refetch);
                        } else if (result.loading) {
                          child = Center(
                            child: CircularProgressIndicator(),
                          );
                        } else {
                          final EventDetailType event =
                              EventDetailType.fromJson(result.data['event']);

                          child = ListView(
                            children: <Widget>[
                              Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.3,
                                width: MediaQuery.of(context).size.width,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: NetworkImage(event.image),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 20),
                                child: Text(
                                  event.title,
                                  textAlign: TextAlign.center,
                                  style:
                                      Theme.of(context).primaryTextTheme.title,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 15),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: <Widget>[
                                    Text(
                                      event.place,
                                      style: Theme.of(context)
                                          .primaryTextTheme
                                          .subtitle,
                                    ),
                                    Text(
                                      "${event.start.day}.${event.start.month}. ${event.start.hour < 10 ? "0" : ""}${event.start.hour}:${event.start.minute < 10 ? "0" : ""}${event.start.minute} - ${event.end.hour < 10 ? "0" : ""}${event.end.hour}:${event.end.minute < 10 ? "0" : ""}${event.end.minute}",
                                      style: Theme.of(context)
                                          .primaryTextTheme
                                          .body1,
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 20),
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.85,
                                    child: Text(
                                      event.detail,
                                      style: Theme.of(context)
                                          .primaryTextTheme
                                          .body1,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          );
                        }
                        return Panel(child);
                      }),
                  ReturnButton(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
