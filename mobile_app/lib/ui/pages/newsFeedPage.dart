import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:mobile_app/models/news.dart';
import 'package:mobile_app/ui/components/news/newsCard.dart';
import 'package:mobile_app/ui/components/news/notificationCard.dart';
import 'package:mobile_app/ui/components/panel/panel.dart';
import 'package:mobile_app/ui/components/panel/panelException.dart';

class NewsFeedPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return NewsFeedPageState();
  }
}

class NewsFeedPageState extends State<NewsFeedPage> {
  final String getLastNotifications = """
    query{
      lastNotifications{
        title
        detail
      }
    }
  """;

  final String getNews = """
    query{
      news{
        title
        image
        detail
      }
    }
  """;

  @override
  Widget build(BuildContext context) {
    return Query(
        options: QueryOptions(
          documentNode: gql(getLastNotifications),
        ),
        builder: (QueryResult notificationResult,
            {VoidCallback refetch, FetchMore fetchMore}) {
          if (notificationResult.hasException) {
            return Panel(PanelException(refetch));
          }
          if (notificationResult.loading) {
            return Panel(Center(
              child: CircularProgressIndicator(),
            ));
          }
          List<NotificationCard> notificationsList = List<NotificationCard>();
          notificationResult.data['lastNotifications'].forEach((notification) =>
              notificationsList.add(
                  NotificationCard(NotificationType.fromJson(notification))));

          return Query(
              options: QueryOptions(
                documentNode: gql(getNews),
              ),
              builder: (QueryResult newsResult,
                  {VoidCallback refetch, FetchMore fetchMore}) {
                if (newsResult.hasException) {
                  return Panel(PanelException(refetch));
                }
                if (newsResult.loading) {
                  return Panel(Center(
                    child: CircularProgressIndicator(),
                  ));
                }
                List<NewsCard> newsList = List<NewsCard>();
                newsResult.data['news'].forEach(
                    (news) => newsList.add(NewsCard(NewsType.fromJson(news))));

                List<Widget> feed = List<Widget>();
                feed.addAll(notificationsList);
                feed.addAll(newsList);
                return Panel(ListView(
                  padding: EdgeInsets.only(top: 30),
                  children: feed,
                ));
              });
        });
  }
}
