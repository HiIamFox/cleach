import 'package:flutter/material.dart';
import 'package:mobile_app/ui/components/header.dart';
import 'package:mobile_app/ui/components/navigation.dart';
import 'package:mobile_app/ui/pages/lineupPage.dart';
import 'package:mobile_app/ui/pages/mapPage.dart';
import 'package:mobile_app/ui/pages/newsFeedPage.dart';

class DefaultPage extends StatefulWidget {
  DefaultPage();

  @override
  _DefaultPageState createState() => _DefaultPageState();
}

class _DefaultPageState extends State<DefaultPage> {
  int selectedIndex = 0;

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final List<Widget> pages = [
    LineupPage(),
    NewsFeedPage(),
    MapPage(),
  ];

  void callback(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Theme.of(context).backgroundColor,
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 20, bottom: 30),
              child: Header(),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 20),
              child:
                  Navigation(selectedIndex: selectedIndex, callback: callback),
            ),
            Expanded(
              flex: 18,
              child: pages[selectedIndex],
            ),
          ],
        ),
      ),
    );
  }
}
