import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:mobile_app/ui/components/lineup/dayMenu.dart';
import 'package:mobile_app/ui/components/panel/panel.dart';
import 'package:mobile_app/models/event.dart';
import 'package:mobile_app/ui/components/lineup/eventCard.dart';
import 'package:mobile_app/ui/components/panel/panelException.dart';

class LineupPage extends StatefulWidget {
  @override
  _LineupPageState createState() => _LineupPageState();
}

class _LineupPageState extends State<LineupPage> {
  final String getEvents = """
    query eventsByDateRange(\$min: Datetime!, \$max: Datetime! ){
      eventsByDateRange(min: \$min, max: \$max){
        uid
        title
        place
        start
        end
      }
    }
  """;
  final days = [
    DateTime(2019, 9, 25),
    DateTime(2019, 9, 26),
    DateTime(2019, 9, 27)
  ];
  int selectedIndex = 0;
  Widget child;

  void callback(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Padding(
        padding: EdgeInsets.only(bottom: 20),
        child: DayMenu(
          selectedIndex: selectedIndex,
          callback: callback,
        ),
      ),
      Query(
          options: QueryOptions(documentNode: gql(getEvents), variables: {
            'min': days[selectedIndex].toIso8601String(),
            'max': DateTime(days[selectedIndex].year, days[selectedIndex].month,
                    days[selectedIndex].day + 1)
                .toIso8601String()
          }),
          builder: (QueryResult result,
              {VoidCallback refetch, FetchMore fetchMore}) {
            if (result.hasException) {
              child = PanelException(refetch);
            } else if (result.loading) {
              child = Center(
                child: CircularProgressIndicator(),
              );
            } else {
              final List<EventListType> events = List<EventListType>();
              final List<EventCard> eventCards = List<EventCard>();
              result.data['eventsByDateRange']
                  .forEach((item) => events.add(EventListType.fromJson(item)));
              events.forEach((event) => eventCards.add(EventCard(event)));
              child = ListView(
                padding: EdgeInsets.only(top: 30),
                children: eventCards,
              );
            }
            return Expanded(
              child: Panel(child),
            );
          }),
    ]);
  }
}
