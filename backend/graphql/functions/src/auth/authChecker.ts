import getAuthToken from "./getAuthToken";
import verifyToken from "./verifyToken";
import { auth } from "firebase-admin";

const authChecker = async ({ context: { req } }, roles: any) => {
  const jwt = getAuthToken(req);
  const decodedToken: auth.DecodedIdToken = await verifyToken(jwt);
  if (!!!decodedToken) {
    return false;
  } else if (roles.length === 0) {
    // if `@Authorized()`, check only if user exist
    return true;
  }
  // check roles
  else if (decodedToken.role) {
    if (decodedToken.role === "MODERATOR" && roles.includes("MODERATOR")) {
      return true;
    } else if (decodedToken.role === "ADMIN" && roles.includes("ADMIN")) {
      return true;
    }
  }
  return false;
};

export default authChecker;
