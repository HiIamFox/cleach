import { Request } from "express";

/**
 * Returns authorization token from authorization header
 *
 * @param {Context} {req}
 * @returns {string}
 */
const getAuthToken = (req:Request): string => {
  if (
    req.headers.authorization &&
    req.headers.authorization.split(" ")[0] === "Bearer"
  ) {
    return req.headers.authorization.split(" ")[1];
  }
  return null;
};
export default getAuthToken;