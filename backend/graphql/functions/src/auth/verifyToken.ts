import admin from "firebase-admin";

/**
 * Verify acquired jwt
 *
 * @param {string} idToken
 * @returns {Promise<admin.auth.DecodedIdToken>}
 */
const verifyToken = async (
  idToken: string
): Promise<admin.auth.DecodedIdToken> => {
  const decoded = await admin.auth().verifyIdToken(idToken);
  return decoded;
};

export default verifyToken;
