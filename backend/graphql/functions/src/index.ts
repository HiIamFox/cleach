import "reflect-metadata"; // tslint:disable-line
import { buildSchema } from "type-graphql";

/* Firebase imports */
import * as admin from "firebase-admin";
import * as functions from "firebase-functions";
import * as serviceAccount from "./serviceKey.json";
/* /Firebase imports */

/* Apollo express server */
import express from "express";
import cors from "cors";
import { ApolloServer } from "apollo-server-express";
/* /Apollo express server */

/* Graphql resolvers */
import EventResolver from "./resolvers/eventResolver";
import UserResolver from "./resolvers/userResolver";
import RatingResolver from "./resolvers/ratingResolver";
import NewsResolver from "./resolvers/newsResolver";
import NotificationResolver from "./resolvers/notificationResolver";
/* /Graphql resolvers */

import authChecker from "./auth/authChecker";

admin.initializeApp({
  credential: admin.credential.cert({
    privateKey: serviceAccount.private_key,
    clientEmail: serviceAccount.client_email,
    projectId: serviceAccount.project_id
  }),
  databaseURL: "https://cleach-festival.firebaseio.com",
  storageBucket: "gs://cleach-festival.appspot.com/"
});

const app = express();
const path = "/";

const main = async () => {
  const schema = await buildSchema({
    resolvers: [
      EventResolver,
      UserResolver,
      RatingResolver,
      NewsResolver,
      NotificationResolver
    ],
    authChecker,
    validate: false
  });

  const server = new ApolloServer({
    schema,
    /* Playground settings */
    playground: true,
    introspection: true,
    /* /Playground settings*/
    context: ({ req }: any) => ({ req })
  });

  app.set("trust proxy", 1);
  app.use(
    cors({
      origin: "*"
    })
  );

  server.applyMiddleware({
    app,
    path
  });
};

main()
  .then(v => console.log("API ready"))
  .catch(err => console.error("There was an error", err));

export const graphql = functions.https.onRequest(app);

const options: any = {
  action: "read",
  expires: "01-01-2025"
};

export const eventImages = functions.storage
  .object()
  .onFinalize(async event => {
    const fileName = event.name;
    const file = admin
      .storage()
      .bucket()
      .file(fileName);
    if (!file.isPublic) await file.makePublic();
    file
      .getSignedUrl(options)
      .then(url => console.log(url[0]))
      .catch(error => console.log(error));
  });
