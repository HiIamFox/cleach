export enum UserRole{
    DEFAULT = "DEFAULT",
    MODERATOR = "MODERATOR",
    ADMIN = "ADMIN"
}