import { ObjectType, Field } from "type-graphql";

@ObjectType()
class User {
  @Field()
  uid: string;

  @Field({ nullable: true })
  displayName?: string;

  @Field()
  email: string;

  @Field()
  role: string;

  @Field()
  disabled: boolean;
}

export default User;
