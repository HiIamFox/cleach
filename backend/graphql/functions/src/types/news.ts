import { ObjectType, Field } from "type-graphql";
import Datetime from "./scalar/dateScalar";

@ObjectType()
class News {
  @Field()
  uid: string;

  @Field()
  title: string;

  @Field({ nullable: true })
  image?: string;

  @Field()
  detail: string;

  @Field(type => Datetime)
  createdAt: Date;
}

export default News;
