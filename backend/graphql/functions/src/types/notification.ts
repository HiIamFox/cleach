import { ObjectType, Field } from "type-graphql";
import Datetime from "./scalar/dateScalar";

@ObjectType()
class Notification {
  @Field()
  uid: string;

  @Field()
  title: string;

  @Field()
  detail: string;

  @Field(type => Datetime)
  createdAt: Date;
}

export default Notification;
