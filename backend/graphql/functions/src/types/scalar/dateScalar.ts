import { GraphQLScalarType, Kind } from "graphql";
import { Timestamp } from "@google-cloud/firestore";

const Datetime = new GraphQLScalarType({
    name: "Datetime",
    description: 'Firebase Timestamp',
    parseValue(value) {
        return Timestamp.fromDate(new Date(value));
    },
    serialize(value: Timestamp) {
        return value.toDate();
    },
    parseLiteral(ast) {
        if (ast.kind === Kind.STRING) {
            return Timestamp.fromDate(new Date(ast.value));
        }
        return null;
    }
});
export default Datetime;