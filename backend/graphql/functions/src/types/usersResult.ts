import { ObjectType, Field } from "type-graphql";
import User from "./user";

@ObjectType()
class UsersResult {
  @Field()
  nextPageToken?: string;

  @Field(() => [User])
  users: User[];
}

export default UsersResult;
