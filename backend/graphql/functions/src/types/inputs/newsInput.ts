import { InputType, Field } from "type-graphql";
import News from "../news";

@InputType()
class NewsInput implements Partial<News>{
    
  @Field()
  title: string;

  @Field({ nullable: true })
  image?: string;

  @Field()
  detail: string;
}

export default NewsInput;
