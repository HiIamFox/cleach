import { InputType, Field } from "type-graphql";
import {Rating} from "../rating";

@InputType()
class RatingInput implements Partial<Rating>{
    @Field()
    userUid: string;

    @Field()
    eventUid: string;

    @Field()
    value: number;

    @Field()
    comment?: string;
}

export default RatingInput;