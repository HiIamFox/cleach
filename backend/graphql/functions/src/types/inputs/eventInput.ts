import { InputType, Field } from "type-graphql";
import Event from "../event";
import Datetime from "../scalar/dateScalar";

@InputType()
class EventInput implements Partial<Event>{

    @Field()
    title: string;

    @Field()
    place: string;

    @Field()
    image: string;

    @Field(type => Datetime)
    end: Date;

    @Field(type => Datetime)
    start: Date;

    @Field()
    detail: string;

}

export default EventInput;