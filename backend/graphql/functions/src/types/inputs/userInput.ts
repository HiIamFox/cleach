import { Field, InputType } from "type-graphql";
import User from "../user";

@InputType()
class UserInput implements Partial<User> {
  @Field()
  uid: string;

  @Field({ nullable: true })
  displayName?: string;

  @Field()
  email: string;

  @Field()
  role: string;

  @Field()
  disabled: boolean;
}

export default UserInput;
