import { InputType, Field } from "type-graphql";
import Notification from "../notification";

@InputType()
class NotificationInput implements Partial<Notification>{
  @Field()
  title: string;

  @Field()
  detail: string;
}

export default NotificationInput;