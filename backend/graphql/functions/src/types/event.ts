import { ObjectType, Field } from "type-graphql";
import Datetime from "./scalar/dateScalar";

@ObjectType()
class Event {
  @Field()
  uid: string;
  
  @Field()
  title: string;

  @Field()
  place: string;

  @Field()
  image: string;

  @Field(type => Datetime)
  end: Date;

  @Field(type => Datetime)
  start: Date;

  @Field()
  detail: string;

}
export default Event;