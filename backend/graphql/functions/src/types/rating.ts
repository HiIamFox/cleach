import { ObjectType, Field } from "type-graphql";
import Datetime from "./scalar/dateScalar";

@ObjectType()
export class Rating {
  @Field()
  uid: string;

  @Field()
  userUid: string;

  @Field()
  eventUid: string;

  @Field()
  value: number;

  @Field()
  comment: string;

  @Field(() => Datetime)
  createdAt: Date;
}

@ObjectType()
export class RatingOutput {
  @Field()
  uid: string;

  @Field({nullable: true})
  displayName: string;

  @Field()
  eventUid: string;

  @Field()
  value: number;

  @Field()
  comment: string;

  @Field(() => Datetime)
  createdAt: Date;
}
