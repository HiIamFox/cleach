import { Field, ArgsType } from "type-graphql";
import Datetime from "../scalar/dateScalar";

@ArgsType()
export class GetEventArgs {

    @Field(type => String)
    id: string;
    
}

@ArgsType()
export class GetEventByDateRangeArgs{
    @Field(type => Datetime)
    min: Date;

    @Field(type => Datetime)
    max: Date;
}