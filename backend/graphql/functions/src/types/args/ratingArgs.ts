import { ArgsType, Field } from "type-graphql";

@ArgsType()
export class GetRatingsArgs {
  @Field()
  eventUid: string;

  @Field({ defaultValue: 0 })
  offset: number;
}
