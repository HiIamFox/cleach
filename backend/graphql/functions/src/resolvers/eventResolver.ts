import { Query, Resolver, Args, Mutation, Arg, Authorized } from "type-graphql";
import * as admin from "firebase-admin";
import Event from "../types/event";
import { GetEventArgs, GetEventByDateRangeArgs } from "../types/args/eventArgs";
import EventInput from "../types/inputs/eventInput";

@Resolver()
class EventResolver {
  @Query(() => [Event])
  async events(@Arg("offset", { defaultValue: 0 }) offset: number) {
    const query = admin
      .firestore()
      .collection("events")
      .orderBy("start")
      .limit(5)
      .offset(offset);
    return (await query.get()).docs.map(event => {
      const data = event.data();
      const uid = event.id;
      return { ...data, uid } as Event;
    });
  }

  @Query(() => [Event])
  async eventsByDateRange(@Args() { min, max }: GetEventByDateRangeArgs) {
    const snapshot = await admin
      .firestore()
      .collection("events")
      .where("start", ">", min)
      .where("start", "<", max)
      .get();
    return snapshot.docs.map(event => {
      const data = event.data();
      const uid = event.id;
      return { ...data, uid } as Event;
    });
  }

  @Query(() => Event)
  async event(@Args() { id }: GetEventArgs) {
    const snapshot = await admin
      .firestore()
      .collection("events")
      .doc(id)
      .get();
    const data = snapshot.data();
    const uid = snapshot.id;
    return { ...data, uid } as Event;
  }

  @Authorized(["MODERATOR", "ADMIN"])
  @Mutation(() => Event)
  async addEvent(
    @Arg("event")
    { title, detail, end, start, image, place }: EventInput
  ) {
    const docRef = await admin
      .firestore()
      .collection("events")
      .add({
        title,
        detail,
        end,
        start,
        image,
        place
      });
    const data = (await docRef.get()).data();
    const uid = (await docRef.get()).id;
    return { ...data, uid };
  }

  @Authorized(["MODERATOR", "ADMIN"])
  @Mutation(() => Boolean)
  async removeEvent(@Arg("uid") uid: string) {
    try {
      await admin
        .firestore()
        .collection("events")
        .doc(uid)
        .delete();
    } catch (error) {
      return false;
    }
    return true;
  }
}
export default EventResolver;
