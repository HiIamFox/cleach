import { Query, Resolver, Mutation, Arg, Authorized, Args } from "type-graphql";
import * as admin from "firebase-admin";
import RatingInput from "../types/inputs/ratingInput";
import { Rating, RatingOutput } from "../types/rating";
import { GetRatingsArgs } from "../types/args/ratingArgs";

@Resolver()
class RatingResolver {
  /**
   * Creates new rating on specified event
   *
   * @param {RatingInput} { userUid, eventUid, value, comment }
   * @returns
   * @memberof RatingResolver
   */
  @Mutation(() => Rating)
  async addRating(
    @Arg("newRating") { userUid, eventUid, value, comment }: RatingInput
  ) {
    const docRef = await admin
      .firestore()
      .collection("ratings")
      .add({
        userUid,
        eventUid,
        value,
        comment,
        createdAt: new Date()
      } as Rating);
    const data = (await docRef.get()).data();
    const uid = (await docRef.get()).id;
    return { ...data, uid };
  }

  /**
   * Returns paginated ratings for specified event
   *
   * @param {String} eventId
   * @param {number} offset
   * @returns
   * @memberof RatingResolver
   */
  @Query(() => [RatingOutput])
  async getRatings(@Args() { eventUid, offset }: GetRatingsArgs) {
    const query = admin
      .firestore()
      .collection("ratings")
      .where("eventUid", "==", eventUid)
      .offset(offset)
      .limit(10);
    return (await query.get()).docs.map(async rating => {
      const data = rating.data();
      const uid = rating.id;
      const displayName = (await admin.auth().getUser(data.userUid))
        .displayName;
      return {
        uid,
        eventUid: data.eventUid,
        displayName: displayName,
        value: data.value,
        comment: data.comment,
        createdAt: data.createdAt
      };
    });
  }

  /**
   * Remove rating by specified UID
   *
   * @param {Rating} { uid }
   * @returns
   * @memberof RatingResolver
   */
  @Authorized(["MODERATOR", "ADMIN"])
  @Mutation(() => Boolean)
  async removeRating(@Arg("ratingUid") uid: string) {
    try {
      await admin
        .firestore()
        .collection("ratings")
        .doc(uid)
        .delete();
    } catch (error) {
      return false;
    }
    return true;
  }
}

export default RatingResolver;
