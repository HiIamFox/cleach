import { Query, Resolver, Mutation, Authorized, Arg } from "type-graphql";
import Notification from "../types/notification";
import admin from "firebase-admin";
import NotificationInput from "../types/inputs/notificationInput";

@Resolver()
class NotificationResolver {
  fcm = admin.messaging();

  /**
   * Gets 3 last notifications
   *
   * @returns
   * @memberof NotificationResolver
   */
  @Query(() => [Notification])
  async lastNotifications() {
    const query = admin
      .firestore()
      .collection("notifications")
      .orderBy("createdAt", "desc")
      .limit(3);

    return (await query.get()).docs.map(notification => {
      const data = notification.data();
      const uid = notification.id;
      return { ...data, uid };
    });
  }

  /**
   * Store and send new notification
   *
   * @param {NotificationInput} { title, detail }
   * @returns
   * @memberof NotificationResolver
   */
  @Authorized(["MODERATOR", "ADMIN"])
  @Mutation(() => Notification)
  async sendNotification(
    @Arg("notification") { title, detail }: NotificationInput
  ) {
    const docRef = await admin
      .firestore()
      .collection("notifications")
      .add({ title, detail, createdAt: new Date() });
    const data = (await docRef.get()).data();
    const uid = (await docRef.get()).id;
    const payload: admin.messaging.MessagingPayload = {
      notification: {
        title: data.title,
        body: data.detail,
        clickAction: "FLUTTER_NOTIFICATION_CLICK"
      }
    };
    await this.fcm.sendToTopic("important", payload);
    return { ...data, uid };
  }
}

export default NotificationResolver;
