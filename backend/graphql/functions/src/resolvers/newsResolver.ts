import { Query, Resolver, Mutation, Arg, Authorized } from "type-graphql";
import * as admin from "firebase-admin";
import News from "../types/news";
import NewsInput from "../types/inputs/newsInput";

@Resolver()
class NewsResolver {
  @Query(() => [News])
  async news(@Arg("offset", { defaultValue: 0 }) offset: number) {
    const query = admin
      .firestore()
      .collection("news")
      .orderBy("createdAt", 'desc')
      .limit(5)
      .offset(offset);
    return (await query.get()).docs.map(news => {
      const data = news.data();
      const uid = news.id;
      return { ...data, uid };
    });
  }

  /**
   * Creates new news
   *
   * @param {NewsInput} { title, image, detail }
   * @returns
   * @memberof NewsResolver
   */
  @Authorized(["MODERATOR", "ADMIN"])
  @Mutation(() => News)
  async addNews(@Arg("news") { title, image, detail }: NewsInput) {
    const docRef = await admin
      .firestore()
      .collection("events")
      .add({ title, image, detail, createdAt: new Date() });
    const data = (await docRef.get()).data();
    const uid = (await docRef.get()).id;
    return { ...data, uid };
  }
}

export default NewsResolver;
