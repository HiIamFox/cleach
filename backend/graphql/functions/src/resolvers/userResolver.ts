import { Resolver, Authorized, Mutation, Arg, Query } from "type-graphql";
import * as admin from "firebase-admin";
import UsersResult from "../types/usersResult";
import User from "../types/user";
import UserInput from "../types/inputs/userInput";

@Resolver()
class UserResolver {
  /**
   * Give user specified role
   *
   * @param {string} email
   * @param {Context} ctx
   * @returns {Promise<string>}
   * @memberof AuthResolver
   */
  @Authorized("ADMIN")
  @Mutation(() => User)
  async grantUserRole(
    @Arg("email") email: string,
    @Arg("role") role: string
  ): Promise<User> {
    const userRecord = await admin.auth().getUserByEmail(email);
    await admin.auth().setCustomUserClaims(userRecord.uid, {
      role
    });
    return parseUserRecord(await admin.auth().getUserByEmail(email));
  }

  /**
   * Get paginated list of users
   *
   * @param {string} [nextPageToken]
   * @returns {(admin.auth.ListUsersResult | null)}
   * @memberof UserResolver
   */
  @Authorized(["ADMIN", "MODERATOR"])
  @Query(() => UsersResult)
  async getUsers(
    @Arg("nextPageToken", { nullable: true }) nextPageToken?: string
  ): Promise<UsersResult> {
    const listUsersResult = await admin.auth().listUsers(100, nextPageToken);
    let users: User[] = [];
    listUsersResult.users.forEach(
      userRecord => (users = [...users, parseUserRecord(userRecord)])
    );
    const usersResult: UsersResult = {
      users,
      nextPageToken
    };
    return usersResult;
  }

  /**
   * Edit user information
   *
   * @param {User} editedUser
   * @returns {User}
   * @memberof UserResolver
   */
  @Authorized(["ADMIN", "MODERATOR"])
  @Mutation(() => User)
  async editUser(@Arg("editedUser") editedUser: UserInput): Promise<User> {
    const userRecord = await admin.auth().updateUser(editedUser.uid, {
      displayName: editedUser.displayName,
      email: editedUser.email
    });
    return parseUserRecord(userRecord);
  }

  /**
   * Get users name
   *
   * @param {string} userUid
   * @returns {Promise<string>}
   * @memberof UserResolver
   */
  @Query(() => String)
  async getUserName(@Arg("userUid") userUid: string):Promise<string> {
    const userRecord = await admin.auth().getUser(userUid);
    return userRecord.displayName;
  }
}

export default UserResolver;

/**
 * Get user from firebase UserRecord
 *
 * @param {admin.auth.UserRecord} {
 *   uid,
 *   displayName,
 *   email,
 *   customClaims,
 *   disabled
 * }
 * @returns {User}
 */
const parseUserRecord = ({
  uid,
  displayName,
  email,
  customClaims,
  disabled
}: admin.auth.UserRecord): User => {
  return {
    uid,
    displayName,
    email,
    role: !customClaims ? "DEFAULT" : customClaims["role"] || "DEFAULT",
    disabled
  };
};
